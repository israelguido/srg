 <!DOCTYPE html>
<html lang="pt">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>SGR | Sistema de Gerenciamento de Revenda</title>
	<link rel="shortcut icon" href="/sgr/images/sgr.svg" type="image/x-icon">

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
     <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
	
    <!-- bootstrap-progressbar -->
    <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="../vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>
    <link href='http://fonts.googleapis.com/css?family=Arvo' rel='stylesheet' type='text/css'>
    
  </head>

  <body class="nav-md">
	<?php 
        session_start();
	    
        if ($_SESSION["fun_key"] == "" || $_SESSION["usu_key"] == "" )
            header("Location: login.php");
        
        require_once '../connection_bd/mysqli.php'; 
        
        include_once '../dataobject/usuario.php';
        include_once '../dataobject/funcionario.php';
        
        
		
		
	?>
	<style>
		body{
			padding-top:3px;
		}
	</style>
    <div id="wrapper">

        <?php 
        
        // usuario Logado
        $usu_key = $_SESSION["usu_key"];  
        $data_usuario=clsUsuario_x_Funcionario_x_Empresa::getUsuario_x_Funcionario_x_Empresa($usu_key);
        
        //leitura das tabelas do DataGrid
        $sql=clsFuncionario::lstFuncionario($_SESSION['emp_key']);
        $lstUsuario = $MySql->query($sql) OR trigger_error($MySql->error, E_USER_ERROR);
        
        //Funciuonario selecionado
        if(isset($_GET["idFunKey"]))
        {
            $idFunKey = $_GET['idFunKey'];
            $_SESSION['idFunKey']=$idFunKey;
            $data_Funcionario= clsFuncionario::getFuncionario_usuario($idFunKey);
            $idCKey=$data_Funcionario[0]['crg_key'];
            $idGKey=$data_Funcionario[0]['grpusu_key'];
            ?>
            <script language="javascript">
            var idCKey =null;
            var idGKey =null;
            <?php 
            if(!empty($idCKey))
            {
            ?>
              	var idCKey = "<?php echo $idCKey; ?>";
            <?php 
            }
            if(!empty($idGKey))
            {
            ?>
              	var idGKey = "<?php echo $idGKey ?>";
            <?php 
            }
            ?>
            </script>
            <?php 
        }
        
        ?>
		<div class="container body">
      		<div class="main_container">
        		<div class="col-md-3 left_col">
          			<div class="left_col scroll-view">
            			<div class="navbar nav_title" style="border: 0;">
              				<a href="index.html" class="site_title">
              					<img alt="" src="../images/sgr_branco.png">
							</a>
            			</div>
                         <!-- menu profile quick info -->
            			<div class="profile clearfix">
              				<div class="profile_pic">
                				<img src="../images/img.jpg" alt="..." class="img-circle profile_img">
							</div>
							<div class="profile_info">
                				<span>Olá,</span>
                				<h2><?php echo $data_usuario[0]['fun_nome']; ?></h2>
              				</div>
            			</div>
            			
						<!-- Rotina de Montagem do Menu de Usuario -->
            			<?php 
            			
            			include 'menu.php';
            			
            			?>
       				</div>
			        <!-- top navigation -->
					<div class="top_nav">
						<div class="nav_menu">
            				<nav>
              					<div class="nav toggle">
                					<a id="menu_toggle"><i class="fa fa-bars"></i></a>
              					</div>
              					<ul class="nav navbar-nav navbar-right">
                					<li class="">
                  						<a href="javascript:;" class="fa fa-sign-out pull-right" data-toggle="dropdown" aria-expanded="false"></a>
                					</li>
                					<li role="presentation" class="dropdown">
                  						<a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                    						<i class="fa fa-envelope-o"></i>
                    						<span class="badge bg-green">6</span>
                  						</a>
                					</li>
              					</ul>
            				</nav>
						</div>
        			</div>
			        <!-- /top navigation -->>

                    <!-- page content -->
        			<div class="right_col" role="main">
          				<div class="">
            				<div class="clearfix"></div>
            					<div class="row">
              						<div class="col-md-12 col-sm-12 col-xs-12">
                						<div class="x_panel">
                  							<div class="x_title">
                    							<h2>Cadastro Usuário<small></small></h2>
                    							<div class="clearfix"></div>
                  							</div>
                  						     <div class="x_content">
							                    <div class="panel-body"> 	
                                                	<form id="dadosForm" class="dadosForm" action="" method="post">
                                                    	<div class="row">
                                                            <div class="col-lg-8 col-md-8">
                                                            	<div class="form-group">
                                                                    <label>Nome</label>
                                                                    <div class="input-group"> <span class="input-group-addon"><span  <i class="fa fa-text-width  fa-la"></i></span></span>
                                                                        <input type="text" class="form-control" name="nome" id="nome" placeholder="Nome completo" value="<?php echo utf8_encode($data_Funcionario[0]['fun_nome']); ?>" required>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-3 col-md-3">
                                                            	<div class="form-group">
                                                                    <label>CPF</label>
                                                                    <div class="input-group"> <span class="input-group-addon"><span <i class="fa fa-pencil-square-o  fa-la"></i></span></span>
                                                                        <input type="text" class="form-control" name="cpf" id="cpf" onKeyPress="FormataCpf(this,event);" placeholder="CPF" value="<?php echo $data_Funcionario[0]['fun_cpf']; ?>">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-6 col-md-6">
                                                            	<div class="form-group">
                                                                    <label>Email</label>
                                                                    <div class="input-group"> <span class="input-group-addon"><span class="fa fa-envelope-o fa-lg"></span></span>
                                                                        <input type="text" class="form-control" name="email" id="email" placeholder="Email" value="<?php echo $data_Funcionario[0]['fun_email']; ?>" required>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>   
                                                        <div class="row">
                                                            <div class="col-lg-6 col-md-6">
                                                            	<div class="form-group">
                                                                	<label>Cargo</label>
                                                                    <div class="input-group">
                                                                    	<span class="input-group-addon"><span class="fa fa-list fa-lg"></span></span>
                                                                        	<select id="idCrgKey" name="idCrgKey" class="selectpicker form-control" data-live-search="false" placeholder="Cargo" title="Escolha o cargo..."></select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="col-lg-6 col-md-6">
                                                            	<div class="form-group">
                                                                	<label>Grupo</label>
                                                                    <div class="input-group">
                                                                    	<span class="input-group-addon"><span class="fa fa-list fa-lg"></span></span>
                                                                        	<select id="idGrpUsuKey" name="idGrpUsuKey" class="selectpicker form-control" title="Escolha o Grupo de Usuário ..." ></select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            </div>

                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-6 col-md-6">
                                                            	<div class="form-group">
                                                                    <label>Login</label>
                                                                    <div class="input-group"> <span class="input-group-addon"><span class="fa fa-envelope-o fa-lg"></span></span>
                                                                        <input type="text" class="form-control" name="login" id="login" placeholder="login" value="<?php echo $data_Funcionario[0]['usu_nome']; ?>" required>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-4 col-md-4">
                                                            	<div class="form-group">
                                                                    <label>Nova Senha</label>
                                                                    <div class="input-group"> <span class="input-group-addon"><span class="fa fa-key fa-lg"></span></span>
                                                                    <?php  
                                                                    
                                                                        if(!isset($_GET["idFunKey"]))
                                                                        {
                                                                          ?>
                                                                          	<input type="password" class="form-control" name="novaSenha" id="novaSenha" placeholder="Nova Senha">
                                                                          <?php 
                                                                        }
                                                                        else
                                                                        {
                                                                            ?>
                                                                            <input type="password" class="form-control" name="novaSenha" id="novaSenha" placeholder="Nova Senha" disabled>
                                                                            <?php 
                                                                        }
                                                                    ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                       	<?php  
                                                        if(isset($_GET["fun_key"]))
                                                        {
                                                        ?>
															<div class="col-lg-4 col-md-4">
                                                        		<div class="form-group">
                                                            		<label></label>
                                                                	<div class="checkbox">
                                                                		<label>
                                                                			<input type="checkbox" id="chkAlterarSenha" name="chkAlterarSenha"> Alterar Senha
                                                                		</label>
                                                                	</div>
                                                            	</div>
                                                        	</div>
                                                        <?php 
                                                        }
                                                        ?>
                                                        </div>
                                                        <p></p>
                                                        <div class="row">
                                                            <div class="col-lg-12 col-md-12">
                                                            	<div class="form-group">
                                                                    <button type="submit" id="btnSalvarDados" class="btn btn-default"><span class="fa fa-save fa-lg"></span> Salvar Dados</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                        						</div>
                  							</div>  
                						</div>
              						</div>
            					</div>
		                    <!-- Tabela -->	
          					<div class="row">
              					<div class="col-md-12 col-sm-12 col-xs-12">
                					<div class="x_panel">
                  						<div class="x_title">
                    						<h2>Usuários Cadastrados <small></small></h2>
                    						<div class="clearfix"></div>
                  						</div>
                  						<div class="x_content">
                    						<div class="table-responsive">
                    						
                      							<table class="table table-striped jambo_table bulk_action">
                        							<thead>
                          								<tr class="headings">
                            								<th>
                              									<input type="checkbox" id="check-all" class="flat">
                            								</th>
                            								<th class="column-title">Código</th>
                            								<th class="column-title">Nome</th>
                            								<th class="column-title">CPF</th>
                            								<th class="column-title">Email</th>
                            								<th class="column-title">Cargo</th>
                            								<th class="column-title">Grupo Usuário</th>
                            								<th class="column-title no-link last"><span class="nobr"></span></th>
                            								<th class="bulk-actions" colspan="7">
                              									<a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                            								</th>
                          								</tr>
                        							</thead>
                        						<tbody>
                        		                <?php
                        		                $wctr=0;
                        		                while($row = $lstUsuario->fetch_assoc()) 
                                                {   
                                                    $paginaEdita = 'cadastroUsuario.php?idFunKey='.$row['fun_key'];
                                                    
                                                    $fun_key_lst = $row['fun_key'];
                                                    $fun_nome_lst = utf8_encode($row['fun_nome']);
                                                    $fun_cpf_lst = $row['fun_cpf'];
                                                    $fun_email_lst = $row['fun_email'];
                                                    $crg_descricao_lst = utf8_encode($row['crg_descricao']);
                                                    $grpusu_descricao_lst = utf8_encode($row['grpusu_descricao']);
                                                    
                                                    if($wctr==0)
                                                    {
                                                        $wctr=1;
                                                        ?>
                                                        <tr class="even pointer">
                                                        <?php 
                                                    }
                                                    elseif($wctr==1)
                                                    {
                                                        $wctr=0;
                                                        ?>
                                                        <tr class="odd pointer">
                                                        <?php
                                                        
                                                    }
                        							?>
                                                        	<td class="a-center ">
                                                            	<input type="checkbox" class="flat" name="table_records">
                                                            </td>
                                                            <td class="a-right a-right"><?php echo $fun_key_lst; ?></td>
                                                            <td class="a-right a-right"><?php echo $fun_nome_lst; ?></td>
                                                            <td class="a-right a-right"><?php echo $fun_cpf_lst; ?></td>
                                                            <td class="a-right a-right"><?php echo $fun_email_lst; ?></td>
                                                            <td class="a-right a-right"><?php echo $crg_descricao_lst; ?></td>
                                                            <td class="a-right a-right"><?php echo $grpusu_descricao_lst; ?></td>

                                                            <td class="text-center"><a data-placement="top" data-toggle="tooltip" title="Editar Usuário" class="text-center" href="<?php echo $paginaEdita; ?>" > <i class="fa fa-pencil-square fa-2x"></i></a></td>
                                                        </tr>
                                                        <?php 
                                                    }
                                                    ?>
                        							</tbody>
                      							</table>
                    						</div>
					                  	</div>
                					</div>
                					<!--Tabela-->
          						</div>
        					</div>
                            <!-- /page content -->
                         </div>
                    <div>   
                    <!-- footer content -->
        				<footer>
          					<div class="pull-right">
            					Copyright © 2018 <a href="https://www.tatix.com.br">Tatix Comércio e Participações</a>
          					</div>
          					<div class="clearfix"></div>
        				</footer>
                    <!-- /footer content -->
    				</div>
    			</div>
    		</div>
    	</div>
    <!-- MODAL CHAMADO SUCESSO -->
        <div class="modal fade bs-example-modal-sm" id="dadosSuccess" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
          <div class="modal-dialog modal-sm">
            <div class="modal-content">
            	<div class="modal-header">
                	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    
                    <p></p>
                	<h4 class="modal-title text-center" id="myModalLabel"><i class="fa fa-user fa-fw"></i>  Dados alterados com Sucesso!</h4>
              	</div>
            </div>
          </div>
        </div>
        
         <!-- MODAL CHAMADO EROR -->
        <div class="modal fade bs-example-modal-sm" id="dadosError" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
          <div class="modal-dialog modal-sm">
            <div class="modal-content">
            	<div class="modal-header">
                	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    
                    <p></p>
                	<h4 class="modal-title text-center" id="myModalLabel"><i class="fa fa-exclamation fa-fw"></i>  Senha Inválida!</h4>
              	</div>
            </div>
          </div>
        </div>

        <!-- jQuery -->
        <script src="../vendors/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="../vendors/fastclick/lib/fastclick.js"></script>
        <!-- NProgress -->
        <script src="../vendors/nprogress/nprogress.js"></script>
        <!-- Chart.js -->
        <script src="../vendors/Chart.js/dist/Chart.min.js"></script>
        <!-- gauge.js -->
        <script src="../vendors/gauge.js/dist/gauge.min.js"></script>
        <!-- bootstrap-progressbar -->
        <script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
        <!-- iCheck -->
        <script src="../vendors/iCheck/icheck.min.js"></script>
        <!-- Skycons -->
        <script src="../vendors/skycons/skycons.js"></script>
        <!-- Flot -->
        <script src="../vendors/Flot/jquery.flot.js"></script>
        <script src="../vendors/Flot/jquery.flot.pie.js"></script>
        <script src="../vendors/Flot/jquery.flot.time.js"></script>
        <script src="../vendors/Flot/jquery.flot.stack.js"></script>
        <script src="../vendors/Flot/jquery.flot.resize.js"></script>
        <!-- Flot plugins -->
        <script src="../vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
        <script src="../vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
        <script src="../vendors/flot.curvedlines/curvedLines.js"></script>
        <!-- DateJS -->
        <script src="../vendors/DateJS/build/date.js"></script>
        <!-- JQVMap -->
        <script src="../vendors/jqvmap/dist/jquery.vmap.js"></script>
        <script src="../vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
        <script src="../vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
        <!-- bootstrap-daterangepicker -->
        <script src="../vendors/moment/min/moment.min.js"></script>
        <script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
        <!-- Custom Theme Scripts -->
        <script src="../build/js/custom.min.js"></script>
   	
       <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
   	
    	<!-- scrips do sistema -->
        <script src="js/formata.js"></script> 
        
    
        <script src="js/montaComboGrupoUsuario.js"></script>
    
        <script src="js/montaComboCargo.js"></script>
      
         <!-- JAVASCRIPT DA PÁGINA -->
    
        <script>
    		$(function(){
    			
    			$('#chkAlterarSenha').click(function(){
    				
    				if ($(this).is(':checked')) 
    				{
    					$('#novaSenha').prop('disabled', false);
    				} 
    				else 
    				{
    					$('#novaSenha').prop('disabled', true);
    				}
    				
    			});
    			
    			$('#btnSalvarDados').click(function(){
    				
    				var nome 			=  $('#nome').val()
    				var cpf 			=  $('#cpf').val()
    				var email 			=  $('#email').val()
    				var idCrgKey 		=  $('#idCrgKey').val()
    				var idGrpUsuKey 	=  $('#idGrpUsuKey').val()
    				var login 			=  $('#login').val()
    				var senhaAtual 		=  $('#senhaAtual').val()
    				var chkAlterarSenha =  $('#chkAlterarSenha').val()
    				
    				
    				//Valida Campos
    				if (nome == "")
    				{
    					alert('O Campo Nome não pode ficar vazio!');
    					$('#nome').focus();
    					return false;
    				}
    				else if (cpf == "")
    				{
    					alert('O Campo CPF não pode ficar vazio!');
    					$('#cpf').focus();
    					return false;
    				}
    				else if (email == "")
    				{
    					alert('O Campo Email não pode ficar vazio!');
    					$('#email').focus();
    					return false;
    				}
    				else if (idCrgKey == "")
    				{
    					alert('O Campo Cargo não pode ficar vazio!');
    					$('#idCrgKey').focus();
    					return false;
    				}
    				else if (idGrpUsuKey == "")
    				{
    					alert('O Campo Cargo não pode ficar vazio!');
    					$('#idGrpUsuKey').focus();
    					return false;
    				}
    				else if (login == "")
    				{
    					alert('O Campo Login não pode ficar vazio!');
    					$('#login').focus();
    					return false;
    				}
    				else if (senhaAtual == "")
    				{
    					alert('Informe a Senha para salvar as alterações');
    					$('#senhaAtual').focus();
    					return false;
    				}
    				
    				var dados = $("#dadosForm").serialize();

    				$.ajax({                 
    					  type: 'POST',                            
    					  url: 'db/cadastroUsuarioDB.php',                                  
    					  data:  dados,
    					  dataType: 'json',                
    					  success: function(response) {
    					  
    					  if (response[0].alterou)
    					  {
    						  $('#dadosSuccess').modal('show')
    					 
    						 $('#nome').val("")
    						 $('#cpf').val("")
    						 $('#email').val("")
    						 $('#idCrgKey').val("")
    						 $('#idGrpUsuKey').val("")
    						 $('#login').val("")
    						 $('#senhaAtual').val("")
    						 $('#chkAlterarSenha').prop('checked', false);
    					  }
    					  else
    					  {
    						  $('#dadosError').modal('show')
    					  }
    					  
    				  },
    				  error:function(response){
    					  
    					  alert('erro');
    				  }
    				}); 
    				
    				return false;
    			});
    			
    		});
    	</script>
	
  </body>
</html>
