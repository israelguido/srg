<!DOCTYPE html>
<html lang="pt">
<head>

	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SGR | Sistema de Gerenciamento de Revenda</title>
	<link rel="shortcut icon" href="/sgr/images/sgr.svg" type="image/x-icon"/>

    <!-- Bootstrap Core CSS -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
    
   <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
     
    <!-- Custom Fonts -->
    <link href="css/login-adm.css" rel="stylesheet" type="text/css">
    
    <link href='http://fonts.googleapis.com/css?family=Arvo' rel='stylesheet' type='text/css'>
	
</head>

<body>
	<?php
		session_start();
		
		unset($_SESSION['usuarioLogado']);
		unset($_SESSION['idUsuario']);
		unset($_SESSION['idMunicipio']);
	?>
	<div class="container">
    	<div class="row row-centered">
			<div class="col-md-5 col-sm-8 col-xs-10 col-centered">
				<div class="panel panel-login">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-12">
								<a href="#" class="active" id="login-form-link">Autenticação</a>
							</div>
						</div>
						<hr>
                        <div id="login-form-message"></div>
					</div>
					<div class="panel-body">
                        <div id="login-form-load"></div>
						<div class="row">
							<div class="col-lg-12">
								<form id="login-form" action="" method="post" role="form" style="display: block;">
									<div class="input-group">
                                    	<span class="input-group-addon"><i class="fa fa-user fa-lg"></i></span>
										<input type="text" name="usu_nome" id="usu_nome" tabindex="1" class="form-control" placeholder="Usuário" value="">
									</div>
                                    <br>
									<div class="input-group">
                                    	<span class="input-group-addon"><i class="fa fa-unlock-alt fa-lg"></i></span>
										<input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Senha">
									</div>
                                    <br>
									<div class="form-group">
										<div class="row">
											<div class="col-sm-6 col-sm-offset-3">
												<input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-login" value="Entrar">
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    
    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery-1.10.1.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    
    <script>
		$(document).ready(function(){
			
			$('#login-submit').click(function(){
				
					$("#login-form-load").hide();
					$("#login-form-message").hide();
				
					var message = '';
					var usuario = $("#usu_nome").val();
					var senha = $("#password").val();
					
					if (usuario == '')
					{
						message += '<div class="error-notice">';
						message += '<div class="oaerror danger">';
						message += '	Usuário e Senha não pode ser vazio!';
						message +='  </div>';
						message +='</div>';
						
						setTimeout(function() {
							$("#login-form-load").hide();
							$('#login-form-message').html(message).fadeIn(1000); 
						}, 200);
						
						return false
					}
					else if (senha == '')
					{
						
						message += '<div class="error-notice">';
						message += '<div class="oaerror danger">';
						message += '	Usuário e Senha não pode ser vazio!';
						message +='  </div>';
						message +='</div>';
						
						setTimeout(function() {
							$("#login-form-load").hide();
							$('#login-form-message').html(message).fadeIn(1000); 
						}, 200);
						
						return false
					}

					var progress = '';
					progress += '<div class="progress">';
					progress += '  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">';
					progress +=   '</div>';
					progress += '</div>';
					
					setTimeout(function() {
						$('#login-form-load').html(progress).fadeIn(1000); 
					}, 200); 

					var dados = $("#login-form").serialize();
					$.ajax({  
					  type: 'POST',                            
					  url: 'validaLogin.php',                                  
					  data:  dados,
					  dataType: 'json',                
					  success: function(response) {
						  
						  
						  if (response[0].existe)
						  {
							  	message += '<div class="error-notice">';
								message += '<div class="oaerror success">';
								message += '	Redirecionando para o sistema...';
								message +='  </div>';
								message +='</div>';
								
								setTimeout(function() {
									$("#login-form-load").hide();
									$('#login-form-message').html(message).fadeIn(2000); 
								}, 500);
							
								setTimeout(function() {
									location.href="index.php";  // tela principal 
								}, 3000);
						  }
						  else
						  {
							  	message += '<div class="error-notice">';
								message += '<div class="oaerror danger">';
								message += '	Usuário ou Senha inválidas!';
								message +='  </div>';
								message +='</div>';
								
								setTimeout(function() {
									$("#login-form-load").hide();
									$('#login-form-message').html(message).fadeIn(2000); 
								}, 3000);
						  }
						  
					  },
					  error:function(response){
						  
						  alert('erro');
					  }
					}); 
				
				return false;
			});
		});
	</script>
</body>
</html>