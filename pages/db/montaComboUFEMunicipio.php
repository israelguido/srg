<?php
//header( 'Cache-Control: no-cache' );
header( 'Content-type: application/json; charset="utf-8"', true );

session_start();

//CONEXÃO COM DB
require_once('../../connection_bd/mysqli.php');

//verifico se é um post
if (!empty($_GET)) {
    
    $tipoCombo = $_GET['tipoCombo'];
    
    //Tipo S - secao
    //Tipo igual numerico código da secao
    
    if ($tipoCombo == 'S')
    {
        
        $ufe = array();
        $sql = "SELECT * FROM unidadefederativa WHERE ufe_excluido_s_n = 'N' ORDER BY ufe_codigo;";
        
        $myResult = $MySql->query($sql) OR trigger_error($MySql->error, E_USER_ERROR);
        
        if ($myResult->num_rows > 0)
        {
            while($row = $myResult->fetch_assoc())
            {
                $ufe[] = array(
                    'idufecodigo'	=> $row['ufe_codigo'],
                    'ufe_descricao'			=> utf8_encode($row['ufe_descricao'])
                );
            }
            echo( json_encode( $ufe ) );
        }
    }
    else if ($tipoCombo != 'S')
    {
        $mun = array();
         
        $sql = "SELECT DISTINCT
                        municipio.mun_ibge,
                        municipio.mun_descricao
                    FROM
                        unidadefederativa
                        INNER JOIN municipio ON
                            (unidadefederativa.ufe_codigo = municipio.ufe_codigo) AND
                            (municipio.mun_excluido_s_n='N')
                    WHERE
                        unidadefederativa.ufe_codigo = '$tipoCombo'
			        ORDER BY
                    municipio.mun_descricao;";
        
        $myResult = $MySql->query($sql) OR trigger_error($MySql->error, E_USER_ERROR);
        
        if ($myResult->num_rows > 0)
        {
            while($row = $myResult->fetch_assoc())
            {
                $mun[] = array(
                    'idmunibge'	=> $row['mun_ibge'],
                    'mun_descricao'			=> utf8_encode($row['mun_descricao'])
                );
            }
            
            echo( json_encode( $mun ) );
        }
        
        
    }
    
    
}
?>