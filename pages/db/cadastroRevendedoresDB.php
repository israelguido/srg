<?php
date_default_timezone_set('America/Sao_Paulo');

//CONEXÃO COM DB
include_once '../../connection_bd/mysqli.php';
include_once '../../dataobject/revendedores.php';


//Dados do Chamado

if (!empty($_POST)) {
    
	    session_start();
	    
		$emp_key  = $_SESSION["emp_key"];
		$usu_key  = $_SESSION["usu_key"];
		$idRevKey = $_SESSION["idRevKey"];
		$idReeKey = $_SESSION["idReeKey"];
		$idRecKey = $_SESSION["idRecKey"];
		
		//Revendedores
		$rev_razaosocial = $_POST['rev_razaosocial'];
		$rev_apelido = $_POST['rev_apelido'];
		$rev_cnpj = $_POST['rev_cnpj'];
		$rev_inscricaoestadual = $_POST['rev_inscricaoestadual'];
		$rev_inscricaomunicipal = $_POST['rev_inscricaomunicipal'];
		$idGrpRevKey = $_POST['idGrpRevKey'];
		
		//Revendedores Endereço
		$ree_endereco = $_POST['ree_endereco'];
		$ree_numero = $_POST['ree_numero'];
		$ree_complemento = $_POST['ree_complemento'];
		$idufecodigo = $_POST['idufecodigo'];
		$idmunibge = $_POST['idmunibge'];
		$ree_bairro = $_POST['ree_bairro'];
		$cep_codigo = $_POST['cep_codigo'];
		$ree_telefone = $_POST['ree_telefone'];
		$ree_celular = $_POST['ree_celular'];
		$ree_latitude = $_POST['ree_latitude'];
		$ree_longitude = $_POST['ree_longitude'];
		$ree_url = $_POST['ree_url'];
		$ree_email = $_POST['ree_email'];
		
		//Revendedores Contato
		$rec_tipocontato_c_r_t = $_POST['rec_tipocontato_c_r_t'];
		$rec_nome = $_POST['rec_nome'];
		$rec_funcao = $_POST['rec_funcao'];
		$rec_telefone = $_POST['rec_telefone'];
		$rec_ramal = $_POST['rec_ramal'];
		$rec_celular = $_POST['rec_celular'];
		$rec_email = $_POST['rec_email'];
	
		
		if (!empty($idRevKey))
		{
		    //UPDATE Revendedores
		    $key_values = ("rev_Key=".$idRevKey);
		        
		    $fields_values = ("
            rev_razaosocial='".$rev_razaosocial."',
            rev_apelido='".$rev_apelido."',
            rev_cnpj='".$rev_cnpj."',
            rev_inscricaoestadual='".$rev_inscricaoestadual."',
            rev_inscricaomunicipal='".$rev_inscricaomunicipal."',
            grprev_key=$idGrpRevKey"
		        );
	    
		    clsRevendedores::updRevendedores($fields_values,$key_values);
		    
		    
		}else{
		    $sql = "SELECT rev_key FROM revendedores ORDER BY rev_key DESC LIMIT 1";
		    $myResult = $MySql->query($sql) OR trigger_error($MySql->error, E_USER_ERROR);
		    $idUltimo=0;
		    if ($myResult->num_rows > 0)
		    {
		        $row = $myResult->fetch_assoc();
		        $idUltimo = $row['rev_key'] + 1;
	        }
		    else
		    {
		        $idUltimo = 1;
		        
		    }
		    
		    $rev_datacadastramento =  date('Y-m-d', strtotime("+1 days"));	
		    $rev_excluido_s_n='N';	
		    $rev_observacao='';
		    $fields_values = (
		        $emp_key.','.
		        $idUltimo.',"'.
		        $rev_cnpj.'","'.
		        $rev_razaosocial.'","'.
		        $rev_apelido.'","'.
		        $rev_datacadastramento.'","'.
		        $rev_observacao.'","'.
		        $rev_excluido_s_n.'","'.
		        $rev_inscricaoestadual.'","'.
		        $rev_inscricaomunicipal.'","'.
		        $idGrpRevKey.'"'
		        );
		    
		    $data=clsRevendedores::insRevendedores($fields_values);
		    $idRevKey=$data[0]['rev_key'];
		    $_SESSION["idRevKey"]=$idRevKey;
			    
		}
	
		if (!empty($idReeKey))
		{
            //UPDATE
		    $key_values = ("rev_key=".$idRevKey." AND
	                        ree_key=".$idReeKey
		            );
		        
		        $fields_values = ("
                cep_codigo='".$cep_codigo."',
                ree_endereco='".$ree_endereco."',
                ree_numero='".$ree_numero."',
                ree_complemento='".$ree_complemento."',
                ree_bairro='".$ree_bairro."',
        	    mun_ibge='".$idmunibge."',
        	    ufe_codigo='".$idufecodigo."',
                ree_telefone='".$ree_telefone."',
                ree_celular='".$ree_celular."',
                ree_email='".$ree_email."',
                ree_latitude='".$ree_latitude."',
                ree_longitude='".$ree_longitude."',
                ree_url='".$ree_url."',
	            ree_excluido_s_n='N'"
		            );
		        clsRevendedores::updRevendedoresEndereco($fields_values,$key_values);
		        
		        
		        
		}else{

		    $sql = "SELECT ree_key FROM revendedoresendereco ORDER BY ree_key DESC LIMIT 1";
		    $myResult = $MySql->query($sql) OR trigger_error($MySql->error, E_USER_ERROR);
		    if ($myResult->num_rows > 0)
		    {
		        $row = $myResult->fetch_assoc();
		        $idUltimo = $row['ree_key'] + 1;
		    }
		    else
		    {
		        $idUltimo = 1;
		        
		    }
		
		  
		    //INCLUDE
		    $ree_excluido_s_n='N';
		    $ree_localizacao="";
		    $ree_0800="";
		    
		    $ree_excluido_s_n = 'N';
		    $fields_values = (
		        $idRevKey.','.
		        $idUltimo.',"'.
		        $cep_codigo.'","'.
		        $ree_endereco.'","'.
		        $ree_numero.'","'.
		        $ree_complemento.'","'.
		        $ree_bairro.'","'.
		        $idmunibge.'","'.
		        $idufecodigo.'","'.
		        $ree_localizacao.'","'.
		        $ree_telefone.'","'.
		        $ree_celular.'","'.
		        $ree_url.'","'.
		        $ree_email.'","'.
		        $ree_excluido_s_n.'","'.
		        $ree_0800.'","'.
		        $ree_latitude.'","'.
		        $ree_longitude.'"'
		        );
		    
		    $data=clsRevendedores::insRevendedoresEndereco($fields_values);
		    $idReeKey=$data[0]['ree_key'];
		    $_SESSION["idReeKey"]=$idReeKey;

		}

		if (!empty($idRecKey))
		{
		    //UPDATE
		    $key_values = ("rev_key=".$idRevKey." AND
	                        rec_key=".$idRecKey
		        );
		    $rec_rg=""; 
	        $fields_values = ("
                rec_telefone='".$rec_telefone."',
                rec_ramal='".$rec_ramal."',
                rec_nome='".$rec_nome."',
                rec_funcao='".$rec_funcao."',
                rec_email='".$rec_email."',
                rec_funcao='".$rec_funcao."',
                rec_tipocontato_c_r_t='".$rec_tipocontato_c_r_t."'"
	            );
            clsRevendedores::updRevendedoresContato($fields_values,$key_values);
       
		}else{
		    
		    $sql = "SELECT rec_key FROM revendedorescontato ORDER BY rec_key DESC LIMIT 1";
		    $myResult = $MySql->query($sql) OR trigger_error($MySql->error, E_USER_ERROR);
		    if ($myResult->num_rows > 0)
		    {
		        $row = $myResult->fetch_assoc();
		        $idUltimo = $row['rec_key'] + 1;
		    }
		    else
		    {
		        $idUltimo = 1;
		        
		    }
		    
		    //INCLUDE
		    $rec_excluido_s_n='N';$rec_cpf="";$rec_rg="";
		    $fields_values = (
		        $idRevKey.','.
		        $idUltimo.',"'.
		        $rec_cpf.'","'.
		        $rec_rg.'","'.
		        $rec_telefone.'","'.
		        $rec_ramal.'","'.
		        $rec_nome.'","'.
		        $rec_funcao.'","'.
		        $rec_email.'","'.
		        $rec_celular.'","'.
		        $rec_excluido_s_n.'","'.
		        $rec_tipocontato_c_r_t.'"'
		        );
		    
		        $data=clsRevendedores::insRevendedoresContato($fields_values);
		        $idRecKey=$data[0]['rec_key'];
		        $_SESSION["idRecKey"]=$idRecKey;
		        
		}
		$dados = array();
		$dados[] = array(
		    'alterou'	=> true);
		
		$_SESSION["idRevKey"]=null;
		$_SESSION["idRecKey"]=null;
		$_SESSION["idReeKey"]=null;
		
	}
	else
	{
	    $dados = array();
	    $dados[] = array(
	        'alterou'	=> false);
	}
	//retorno para o javaScript
	echo json_encode($dados);
?>