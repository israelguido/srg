<?php
date_default_timezone_set('America/Sao_Paulo');

//CONEXÃO COM DB
include_once '../../connection_bd/mysqli.php';
include_once '../../dataobject/gruporevendedores.php';
include_once '../../dataobject/funcionario.php';
include_once '../../dataobject/funcionario.php';


//Dados do Chamado

if (!empty($_POST)) {
    
	    session_start();
	    
		$emp_key = $_SESSION["emp_key"];
		$usu_key = $_SESSION["usu_key"];
		$idGrpRevKey = $_SESSION["idGrpRevKey"];
		
		$grprev_descricao = utf8_decode($_POST['descr']);
		
		if (!empty($idGrpRevKey))
		{
		    //UPDATE
		    $key_values = ("grprev_key=".$idGrpRevKey);
		        
		    $fields_values = ("
            grprev_descricao='".$grprev_descricao."'"
		        );
		   		    
		    clsgrupoRevendedores::updGrupoRevendedores($fields_values,$key_values);
		}
		else
		{
		    //INSERT;
		    $sql = "SELECT grprev_key FROM gruporevendedores ORDER BY grprev_key DESC LIMIT 1";
		    $result = $MySql->query($sql);
		    $Ultimo = 0;
		    
		    if ($result->num_rows > 0)
		    {
		        $row = $result->fetch_assoc();
		        $Ultimo = $row['grprev_key'] + 1;
		    }
		    else
		    {
		        $Ultimo = 1;
		    }
		    
		    $grprev_excluido_s_n='N';
		    $fields_values = (
		        $emp_key.','.
		        $Ultimo.',"'.
		        $grprev_descricao.'","'.
		        $grprev_excluido_s_n.'"'
		        );

		    $res=clsGrupoRevendedores::insGrupoRevendedores($fields_values);
		    $grprev_key=$res[0]['grprev_key'];
		    
		}
	
		$dados = array();
		$dados[] = array(
		    'alterou'	=> true);
	}
	else
	{
	    $dados = array();
	    $dados[] = array(
	        'alterou'	=> false);
	}
	//retorno para o javaScript
	echo json_encode($dados);
?>