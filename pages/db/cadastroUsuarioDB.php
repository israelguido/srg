<?php

//CONEXÃO COM DB
require_once('../../connection_bd/mysqli.php');
require_once('../../dataobject/usuario.php');
require_once('../../dataobject/funcionario.php');

//Dados do Chamado

date_default_timezone_set('America/Sao_Paulo');
$datacorrente = date('Y-m-d H:i');


if (!empty($_POST)) {
    
	    session_start();
	    
		$emp_key = $_SESSION["emp_key"];
		$idFunKey = $_SESSION["idFunKey"];
		
		$nome = utf8_decode($_POST['nome']);
		$cpf = $_POST['cpf'];
		$email = $_POST['email'];
		$idCrgKey = $_POST['idCrgKey'];
		$idGrpUsuKey = $_POST['idGrpUsuKey'];
		$login = $_POST['login'];
		if(!empty($_POST['senhaAtual']))
		{
		  $senhaAtual = md5($_POST['senhaAtual']);
		}

		if(!empty($idFunKey))
		{
		    $data=clsUsuario_x_Funcionario_x_Empresa::getUsuario_x_Funcionario_x_Empresa_funKey($idFunKey);
		    $idUsuKey=$data[0]['usu_key'];
		}
		if (!empty($idUsuKey))
		{
		    //UPDATE
		    $key_values = ("usu_key=".$idUsuKey);
		        
		    if($senhaAtual)
		    {
		        $fields_values = ("
                usu_nome='".$login."',
                usu_senha='".$senhaAtual."',
                usu_datasenha='".$data->format('Y-m-d H:i:s')."',
                grpusu_key=".$idGrpUsuKey.""
		            );
		    }else{
		        $fields_values = ("
                usu_nome='".$login."',
                grpusu_key=".$idGrpUsuKey.""
		            );
		    }
		    
		    clsUsuario::updUsuario($fields_values,$key_values);
		    
		    $key_values = ("fun_key=".$idFunKey
		        );
		    
		    $fields_values = ("
             fun_nome='".$nome."',
             fun_cpf='".$cpf."',
             crg_key=".$idCrgKey.",
             fun_email='".$email."',
             fun_email_conf='".$email."'"
		        );
		    
		    clsFuncionario::updFuncionario($fields_values,$key_values);
		    
		    
		}
		else
		{
		    //INSERT;
		    
		    $sql = "SELECT usu_key FROM usuario ORDER BY usu_key DESC LIMIT 1";
			    
		    $result = $MySql->query($sql);
		    $Ultimo = 0;
		    
		    if ($result->num_rows > 0)
		    {
		        $row = $result->fetch_assoc();
		        $Ultimo = $row['usu_key'] + 1;
		    }
		    else
		    {
		        $Ultimo = 1;
		    }
		    
		    $usu_senhaativa_s_n='S';
		    $fields_values = (
		        $Ultimo.',"'.
		        $login.'","'.
		        $senhaAtual.'","'.
		        $usu_senhaativa_s_n.'","'.
		        $datacorrente.'","'.
		        $idGrpUsuKey.'","'.
		        $datacorrente.'"'
		        );
		    
		    $res=clsUsuario::insUsuario($fields_values);
		    
		    $$idUsuKey=$res[0]['usu_key'];
		    
		    
		    $sql = "SELECT  fun_key FROM funcionario ORDER BY fun_key DESC LIMIT 1";
		    $result = $MySql->query($sql);
		    $Ultimo = 0;
		    if ($result->num_rows > 0)
		    {
		        $row = $result->fetch_assoc();
		        $Ultimo = $row['fun_key'] + 1;
		    }
		    else
		    {
		        $Ultimo = 1;
		    }
		    
		    $fun_excluido_s_n = 'N';
		    
		    $fields_values = (
		        $Ultimo.',"'.
		        $nome.'","'.
		        $$cpf.'",'.
		        $idCrgKey.',"'.
		        $email.'","'.
		        $email.'","'.
		        $fun_excluido_s_n.'"'
		        );
		    
		    $res=clsFuncionario::insFuncionario($fields_values);
		    $idFunKey=$res[0]['fun_key'];
		    
		    $fields_values = (
		        $idFunKey.','.
		        $idUsuKey.','.
		        $_SESSION['emp_key'].''
		        );
		    clsUsuario_x_Funcionario_x_Empresa::insUsuario_x_Funcionario_x_Empresa($fields_values);
		    
		}
	
		$dados = array();
		$dados[] = array(
		    'alterou'	=> true);
		
	}
	else
	{
	    $dados = array();
	    $dados[] = array(
	        'alterou'	=> false);
	}
	//retorno para o javaScript
	echo json_encode($dados);
?>