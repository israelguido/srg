<?php
//header( 'Cache-Control: no-cache' );
header( 'Content-type: application/json; charset="utf-8"', true );

//CONEXÃO COM DB
require_once('../../connection_bd/mysqli.php');

session_start();

if (!empty($_GET))
{
    $emp_key=$_SESSION['emp_key'];
    $search = $_GET['search'];
    
    $grpusu = array();
    
    $sql = "SELECT
                    grpusu_key, grpusu_descricao
                FROM
                    grupousuario
                WHERE
                    emp_key = $emp_key AND
                    grpusu_excluido_s_n = 'N'; ";
    
    
    $myResult = $MySql->query($sql) OR trigger_error($MySql->error, E_USER_ERROR);
    while($row = $myResult->fetch_assoc())
    {
        $grpusu[] = array(
            'idGrpUsuKey'	=> $row['grpusu_key'],
            'grpusu_descricao'	=> utf8_encode($row['grpusu_descricao'])
        );
    }
    
    echo( json_encode( $grpusu ) );
}
?>