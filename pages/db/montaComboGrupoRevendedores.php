<?php
//header( 'Cache-Control: no-cache' );
header( 'Content-type: application/json; charset="utf-8"', true );

//CONEXÃO COM DB
require_once('../../connection_bd/mysqli.php');

session_start();

if (!empty($_GET))
{
    $emp_key=$_SESSION['emp_key'];
    $search = $_GET['search'];
 
        $grprev = array();
        
        $sql = "SELECT
                    grprev_key, grprev_descricao
                FROM
                    gruporevendedores
                WHERE
                    emp_key = $emp_key AND
                    grprev_excluido_s_n = 'N'; ";

        $myResult = $MySql->query($sql) OR trigger_error($MySql->error, E_USER_ERROR);
        while($row = $myResult->fetch_assoc())
        {
            $grprev[] = array(
                'idGrpRevKey'	=> $row['grprev_key'],
                'grprev_descricao'	=> utf8_encode($row['grprev_descricao'])
            );
        }
    
    echo( json_encode( $grprev ) );
}
?>