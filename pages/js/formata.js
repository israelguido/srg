


function formataCNPJ(Campo,teclapres)
{
  //Chamado no evento ONKEYUP

  var tecla = teclapres.keyCode;
  var vr;

  vr = Campo.value;
  vr = vr.replace(".", "");
  vr = vr.replace(".", "");
  vr = vr.replace(".", "");
  vr = vr.replace("/", "");
  vr = vr.replace("-", "");
  tam = vr.length + 1;

  tam = vr.length;

  if ( tecla != 9 && tecla != 8 )
  {
    if ( tam > 1 && tam < 6 )
      Campo.value = vr.substr( 0, 2  ) + '.' + vr.substr( 2, 3 );
    if ( tam >= 5 && tam < 8 )
      Campo.value = vr.substr( 0, 2 ) + '.' + vr.substr( 2, 3 ) + '.' + vr.substr( 5, 3 );
    if ( tam >= 8 && tam < 13 )
      Campo.value = vr.substr( 0, 2 ) + '.' + vr.substr( 2, 3 ) + '.' + vr.substr( 5, 3 ) + '/' + vr.substr( 8, 4 );
    if ( tam >= 12 && tam <15 )
      Campo.value = vr.substr( 0, 2 ) + '.' + vr.substr( 2, 3 ) + '.' + vr.substr( 5, 3 ) + '/' + vr.substr( 8, 4 ) + '-' + vr.substr(12,2);
  }
}


	// if (tam == 11)
	//   campo.value = vr.substr(0, 2) + '-' + vr.substr(4, 7) + '-' + vr.substr(10, 4) + '.';

//Formatador de CEP
function FormataCEP(campo, teclaPress) {
  if (window.event){
    var tecla = teclaPress.keyCode;
  } else {
    tecla = teclaPress.which;
  }
  var s = new String(campo.value);
  s = s.replace(/(\.|\(|\)|\/|\-| )+/g,'');
  tam = s.length + 1;
  if (tam > 5 && tam < 7)
    campo.value = s.substr(0,5) + '-' + s.substr(5, tam);
}

function FormataCpf(campo, teclapres)
{
	var tecla = teclapres.keyCode;
	var vr = new String(campo.value);
	vr = vr.replace(".", "");
	vr = vr.replace(".", "");
	vr = vr.replace("-", "");
			
	tam = vr.length + 1;
	if (tecla != 14)
	{
	 if (tam == 4)
	   campo.value = vr.substr(0, 3) + '.';
	 if (tam == 7)
	   campo.value = vr.substr(0, 3) + '.' + vr.substr(3, 3) + '.';
	 if (tam == 10)
	   campo.value = vr.substr(0, 3) + '.' + vr.substr(3, 3) + '.' + vr.substr(6, 3) + '-';
	 if (tam == 14)
	   campo.value = vr.substr(0, 3) + '.' + vr.substr(3, 3) + '.' + vr.substr(6, 3) + '-' + vr.substr(10, 2);
	 
	}
}


function formatTelefone(element, e){
  if (e.keyCode != 9){
    length = element.value.length;
    if (length == 2){
      if (element.value.charAt(0)!= "(")
        element.value = "(" + element.value + ")";
    }
    if (length == 3)
      if (element.value.charAt(0)== "(")
        element.value += ")";
    if (length == 9)
      element.value += "-";
  }
}



function formatCelular(element, e){
  if (e.keyCode != 9){
    length = element.value.length;
    if (length == 2){
      if (element.value.charAt(0)!= "(")
        element.value = "(" + element.value + ")";
    }
    if (length == 3)
      if (element.value.charAt(0)== "(")
        element.value += ")";
    if (length == 9)
      element.value += "-";
  }
}

function formatadata(Campo, teclapres){
	var tecla = teclapres.keyCode;
	var vr = new String(Campo.value);
    vr = vr.replace( "/\", \"\"" );
	tam = vr.length + 1;
	if (tecla != 8 && tecla != 8)
	{
		if (tam > 0 && tam < 2)
			Campo.value = vr.substr(0, 2) ;
		if (tam > 2 && tam < 4)
			Campo.value = vr.substr(0, 2) + '/' + vr.substr(2, 2);
		if (tam > 4 && tam < 7)
			Campo.value = vr.substr(0, 2) + '/' + vr.substr(2, 2) + '/' + vr.substr(4, 7);
	}
}

//Formatador de Numero do Processo
function FormataNrProcesso(campo, teclaPress) {
  if (window.event){
    var tecla = teclaPress.keyCode;
  } else {
    tecla = teclaPress.which;
  }
  var s = new String(campo.value);
  s = s.replace(/(\.|\(|\)|\/|\-| )+/g,'');
  tam = s.length + 1;
  if (tam > 4 && tam < 8)
    campo.value = s.substr(0,4) + '/' + s.substr(4, tam);
  if (tam > 8 && tam < 10)
    campo.value = s.substr(0,4) + '/' + s.substr(4, 8)+ '-' + s.substr(8, tam);

    var novoTexto = campo.value.toUpperCase();
	campo.value = novoTexto;
}

//verifica senha digitada

function securepass(_value) {
    
    //+-------------------------+
    //| Declaração de variaveis |
    //+-------------------------+
    var level = 0;
    var value = _value.replace(/\s/g,'');
    var lowerCase = value.search(/[a-z]/);
    var upperCase = value.search(/[A-Z]/);
    var numbers = value.search(/[0-9]/);
    var specialChars = value.search(/[@!#$%&*+=?|-]/);
        var aLowerCase = value.split(/[a-z]/);
        var aUpperCase = value.split(/[A-Z]/);
        var aNumbers = value.split(/[0-9]/);
        var aSpecialChars = value.split(/[@!#$%&*+=?|-]/) ;
            
            //+------------------------------------+
            //| Verifica o nivel da senha digitada |
            //+------------------------------------+
            if (_value.length!=0) {
                if (lowerCase>=0) {level += 10;}
                if (upperCase>=0) {level += 10;}
                if (numbers>=0) {level += 10;}
                if (specialChars>=0) {level += 10;}
                
                if (aLowerCase.length>2) {level += 5;}
                if (aUpperCase.length>2) {level += 5;}
                if (aNumbers.length>2) {level += 5;}
                if (aSpecialChars.length>2) {level += 10;}
                
                if (value.length >= 4) {level += 5;}
                if (value.length >= 6) {level += 10;}
                if (value.length > 8) {level += 20;}
            }
            
            return level;
}

