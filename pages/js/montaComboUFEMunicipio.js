// JavaScript Document
$(function(){
	$.getJSON('db/montaComboUFEMunicipio.php?search=',{tipoCombo: 'S', ajax: 'true'}, function(j){
		//var options = '<select id="idufecodigo" name="idufecodigo" class="selectpicker form-control" data-live-search="false">';
		var options = '<option value="0"></option>';
		for (var i = 0; i < j.length; i++) {
			options += '<option data-icon="glyphicon-record" value="' + j[i].idufecodigo + '">' + j[i].ufe_descricao + '</option>';
		}	
		//options += '</div>';
		$('#idufecodigo').html(options);
		
		$('#idufecodigo').selectpicker('refresh');
		$('.selectpicker').selectpicker('setStyle', 'btn-default');
	});
	$('#idufecodigo').change(function(){
	
		if( $(this).val() ) {
			$.getJSON('db/montaComboUFEMunicipio.php?search=',{tipoCombo: $(this).val(), ajax: 'true'}, function(j){
				var options = '<option value="0"></option>';	
				for (var i = 0; i < j.length; i++) {

					options += '<option data-icon="glyphicon-record" value="' + j[i].idmunibge + '"> ' +  j[i].mun_descricao + '</option>';
				}	
				$('#idmunibge').html(options);
				
				$('#idmunibge').selectpicker('refresh');
				$('.selectpicker').selectpicker('setStyle', 'btn-default');
			});
		} else {
			$('#idmunibge').html('<option value="">-- Escolha um estado --</option>');
		}
	});
	
});

