// JavaScript Document
$(function(){
	$.getJSON('db/montaComboUFEMunicipio.php?search=',{tipoCombo: 'S', ajax: 'true'}, function(j){
		//var options = '<select id="idufecodigo" name="idufecodigo" class="selectpicker form-control" data-live-search="false">';
		var options = '<option value="0"></option>';
		for (var i = 0; i < j.length; i++) {
			options += '<option data-icon="glyphicon-record" value="' + j[i].idufecodigo + '">' + j[i].ufe_descricao + '</option>';
		}	
		//options += '</div>';
		$('#idufecodigo').html(options);
		$('#idufecodigo').selectpicker('refresh');
		$('.selectpicker').selectpicker('setStyle', 'btn-default');
	});
	$('#idufecodigo').change(function(){
		if( $(this).val() ) {
			$.getJSON('db/montaComboUFEMunicipio.php?search=',{tipoCombo: $(this).val(), ajax: 'true'}, function(j){
				var checks0;var checks1;var checks2;
				var qtd;var res;
				$('#formcheckbox').val("")
				
				var qtd=$(j).size()
				
				var res=(qtd/3);
				
				var res = res.toFixed(0);
				
				//alert(res);
				
				var ctr=0;
				for (var ii = 0; ii < 3; ii++) {
					
					if(ii==0){
						for (var i = 0; i < res; i++) {
							
							checks0  += '<input type="checkbox" nome="'+'id'+ j[i].idmunibge +'" value="'+ j[i].idmunibge +'">' +  j[i].mun_descricao +'</br>';
						}
						$('#formcheckbox0').append(checks0);
					}
					if(ii==1){
						for (var i = res; i < res*2; i++) {
							
							checks1  += '<input type="checkbox" nome="'+'id'+ j[i].idmunibge +'" value="'+ j[i].idmunibge +'">' +  j[i].mun_descricao +'</br>';
						}
						$('#formcheckbox1').append(checks1);
					}
					if(ii==2){
						for (var i = res*2; i < res*3; i++) {
							
							checks2  += '<input type="checkbox" nome="'+'id'+ j[i].idmunibge +'" value="'+ j[i].idmunibge +'">' +  j[i].mun_descricao +'</br>';
						}
						
						$('#formcheckbox2').append(checks2);	
					}
				}
				$('.selectpicker').selectpicker('setStyle', 'btn-default');
			});
		} else {
			$('#idmunibge').html('<option value="">-- Escolha um estado --</option>');
		}
	});
	
});

