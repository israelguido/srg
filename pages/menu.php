    <?php

    session_start();

	require_once'../connection_bd/mysqli.php';
			
	$usu_key = $_SESSION['usu_key'];
		
	$sql="
        SELECT DISTINCT 
        	modulo.* 
        FROM 
        	usuario 
        	INNER JOIN grupousuario ON 
            	(usuario.grpusu_key = grupousuario.grpusu_key) 
            INNER JOIN grupousuario_x_formulario ON 
            	(grupousuario.grpusu_key = grupousuario_x_formulario.grpusu_key) 
            INNER JOIN formulario ON 
            	(grupousuario_x_formulario.form_key = formulario.form_key) 
            INNER JOIN modulo ON 
            	(formulario.mdl_key = modulo.mdl_key) 
        WHERE 
            usuario.usu_key = $_SESSION[usu_key] AND
            usuario.usu_senhaativa_s_n = 'S'
        ORDER BY
            modulo.mdl_key";
	
	$menu = $MySql->query($sql) OR trigger_error($MySql->error, E_USER_ERROR);
	?>
	
    <!-- /menu profile quick info -->
	<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    	<div class="menu_section">
        	<ul class="nav side-menu">
            <?php
                while($row = $menu->fetch_assoc()) 
    			{
                    $mdl_key = $row['mdl_key'];
    				$mdl_descricao = $row['mdl_descricao'];
					$mdl_ajuste = '          ';
    				$mdl_icon = $row['mdl_icon'];
    				?>
    				 <li>
    				 	<a>
    				 		<i class="fa fa-<?php echo $mdl_icon;?> fa-lg"></i> <?php echo utf8_encode($mdl_descricao); ?> <?php echo utf8_encode($mdl_ajuste); ?>
    				 		<span class="fa fa-chevron-down"></span>
    				 	</a>
                        <ul class="nav child_menu">
                        	<?php 
                           
                            $sql_form="
                                SELECT 
                                	formulario.*
                                FROM
                                	usuario
                                	INNER JOIN grupousuario ON
                                    	(usuario.grpusu_key = grupousuario.grpusu_key)
                                    INNER JOIN grupousuario_x_formulario ON
                                    	(grupousuario.grpusu_key = grupousuario_x_formulario.grpusu_key)
                                    INNER JOIN formulario ON
                                    	(grupousuario_x_formulario.form_key = formulario.form_key)
                                    INNER JOIN modulo ON
                                    	(formulario.mdl_key = modulo.mdl_key) AND
                                        (modulo.mdl_key = $mdl_key)
                                WHERE
                                    usuario.usu_key = $_SESSION[usu_key] AND
                                    usuario.usu_senhaativa_s_n = 'S'
                                ORDER BY
                                	modulo.mdl_key";
                            $menu_form = $MySql->query($sql_form) OR trigger_error($MySql->error, E_USER_ERROR);
                            while($row_form = $menu_form->fetch_assoc())
                            {
                                $form_key = $row_form['form_key'];
                                $form_titulo = $row_form['form_titulo'];
                                $form_href = $row_form['form_href'];
                                ?>
									<li>
                                		<a href="<?php echo $form_href; ?>"> <?php echo utf8_encode($form_titulo); ?></a>
									</li>
								<?php
                            }   
                            ?>
                		</ul>
                    </li>
                    <?php 
    			}
    		?>
			</ul>
        </div>
        <!-- /menu footer buttons -->
        <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="">
            	<span class="glyphicon glyphicon-fullscreen"aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="">
            	<span class="glyphicon glyphicon-stop"aria-hidden="true"></span>
            </a>
        	<a data-toggle="tooltip" data-placement="top" title="Settings">
				<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
			</a>
            <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.php">
            	<span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
		</div>
        <!-- /menu footer buttons -->
    </div>
    <!-- /.navbar-static-side -->
</div>
