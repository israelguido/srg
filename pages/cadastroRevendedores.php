 <!DOCTYPE html>
<html lang="pt">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>SGR | Sistema de Gerenciamento de Revenda</title>
	<link rel="shortcut icon" href="/sgr/images/sgr.svg" type="image/x-icon">

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
     
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
	
    <!-- bootstrap-progressbar -->
    <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="../vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>
    <link href='http://fonts.googleapis.com/css?family=Arvo' rel='stylesheet' type='text/css'>
    
  </head>

  <body class="nav-md">
	<?php 
        session_start();
	    
        if ($_SESSION["fun_key"] == "" || $_SESSION["usu_key"] == "" )
            header("Location: login.php");
        
        require_once '../connection_bd/mysqli.php'; 
        
        include_once '../dataobject/revendedores.php';
        include_once '../dataobject/funcionario.php';
        include_once '../dataobject/usuario.php';
	
	?>
	<style>
		body{
			padding-top:3px;
		}
	</style>
	<div id="wrapper">

        <?php 
        
        // usuario Logado
        $usu_key = $_SESSION["usu_key"];  
        $data_usuario=clsUsuario_x_Funcionario_x_Empresa::getUsuario_x_Funcionario_x_Empresa($usu_key);
        
        //leitura das tabelas do DataGrid
        $sql=clsRevendedores::lstRevendedores($_SESSION['emp_key']);
        $lstRevendedores = $MySql->query($sql) OR trigger_error($MySql->error, E_USER_ERROR);
        
        $idGKey=null;$idUKey=null;$idMKey=null;
        
        //Revendedores selecionado
        if(isset($_GET["idRevKey"]))
        {
            $idRevKey = $_GET['idRevKey'];
            $data_Revendedores=clsRevendedores::getRevendedores($idRevKey);
            $_SESSION["idRevKey"]=$idRevKey;
            $idGKey = $data_Revendedores[0]['grprev_key'];
            
            
            $data_RevendedoresEndereco=clsRevendedores::getrevendedoresEndereco_revkey($idRevKey);
            $_SESSION["idReeKey"]=$data_RevendedoresEndereco[0]['ree_key'];
            $idUKey = $data_RevendedoresEndereco[0]['ufe_codigo'];
            $idMKey = $data_RevendedoresEndereco[0]['mun_ibge'];
              
            $data_RevendedoresContato=clsRevendedores::getrevendedoresContato_revkey($idRevKey);
            $_SESSION["idRecKey"]=$data_RevendedoresContato[0]['rec_key'];
        }else{
            $_SESSION['idRevKey']=null;
            $_SESSION['idReeKey']=null;
            $_SESSION['idRecKey']=null;
        }
        ?>
        <script language="javascript">
        	var idGKey = "<?php echo $idGKey; ?>";
            var idUKey = "<?php echo $idUKey; ?>";
            var idMKey = "<?php echo $idMKey; ?>";
        </script>
		<div class="container body">
      		<div class="main_container">
        		<div class="col-md-3 left_col">
          			<div class="left_col scroll-view">
            			<div class="navbar nav_title" style="border: 0;">
              				<a href="index.html" class="site_title">
              					<img alt="" src="../images/sgr_branco.png">
							</a>
            			</div>
                         <!-- menu profile quick info -->
            			<div class="profile clearfix">
              				<div class="profile_pic">
                				<img src="../images/img.jpg" alt="..." class="img-circle profile_img">
							</div>
							<div class="profile_info">
                				<span>Olá,</span>
                				<h2><?php echo $data_usuario[0]['fun_nome']; ?></h2>
              				</div>
            			</div>
            			
						<!-- Rotina de Montagem do Menu de Usuario -->
            			<?php 
            			
            			include 'menu.php';
            			
            			?>
       				</div>
			        <!-- top navigation -->
					<div class="top_nav">
						<div class="nav_menu">
            				<nav>
              					<div class="nav toggle">
                					<a id="menu_toggle"><i class="fa fa-bars"></i></a>
              					</div>
              					<ul class="nav navbar-nav navbar-right">
                					<li class="">
                  						<a href="javascript:;" class="fa fa-sign-out pull-right" data-toggle="dropdown" aria-expanded="false"></a>
                					</li>
                					<li role="presentation" class="dropdown">
                  						<a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                    						<i class="fa fa-envelope-o"></i>
                    						<span class="badge bg-green">6</span>
                  						</a>
                					</li>
              					</ul>
            				</nav>
						</div>
        			</div>
			        <!-- /top navigation -->>

                    <!-- page content -->
        			<div class="right_col" role="main">
          				<div class="">
            				<div class="clearfix"></div>
            				<div class="row">
              					<div class="col-md-12 col-sm-12 col-xs-12">
                					<div class="x_panel">
                  						<div class="x_title">
                    						<h2>Cadastro Revendedores<small></small></h2>
                    						<div class="clearfix"></div>
                  						</div>
                                        <form id="dadosForm" class="dadosForm" action="" method="post">
                                        	<div class="tabbable">
                                            	<ul class="nav nav-tabs">
                                                	<li class="active"><a href="#tab1" data-toggle="tab">Dados Básicos</a></li>
                                                    <li><a href="#tab2" data-toggle="tab">Endereço</a></li>
                                                    <li><a href="#tab3" data-toggle="tab">Contato</a></li>
                                                </ul>
                                                <div class="tab-content">
                                                	<div class="tab-pane active" id="tab1">
                                                    <!-- dados da tab1 --> 
                           	                        	<div class="row">
                                                        	<div class="col-lg-8 col-md-8">
                                                            	<div class="form-group">
                                                                	<label>Razão Social</label>
                                                                    <div class="input-group"> <span class="input-group-addon"><span  <i class="fa fa-text-width fa-la"></i></span></span>
                                                                    	<input type="text" class="form-control" name="rev_razaosocial" id="rev_razaosocial" placeholder="Razão Social" value="<?php echo utf8_encode($data_Revendedores[0]['rev_razaosocial']); ?>" required>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-3 col-md-3">
                                                            	<div class="form-group">
                                                                	<label>Nome Fantasia</label>
                                                                    <div class="input-group"> <span class="input-group-addon"><span  <i class="fa fa-text-width fa-la"></i></span></span>
                                                                    	<input type="text" class="form-control" name="rev_apelido" id="rev_apelido" placeholder="Nome Fantasia" value="<?php echo utf8_encode($data_Revendedores[0]['rev_apelido']); ?>">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
														<div class="row">
                                                        	<div class="col-lg-3 col-md-3">
                                                            	<div class="form-group">
                                                                	<label>CNPJ</label>
                                                                    <div class="input-group"> <span class="input-group-addon"><span <i class="fa fa-pencil-square-o  fa-la"></i></span></span>
                                                                    	<input type="text" class="form-control" name="rev_cnpj" id="rev_cnpj" onKeyPress="formataCNPJ(this,event);" placeholder="CNPJ " value="<?php echo $data_Revendedores[0]['rev_cnpj']; ?>" required>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4">
                                                            	<div class="form-group">
                                                                	<label>Inscrição Estadual</label>
                                                                    <div class="input-group"> <span class="input-group-addon"><span <i class="fa fa-pencil-square-o  fa-la"></i></span></span>
                                                                    	<input type="text" class="form-control" name="rev_inscricaoestadual" id="rev_inscricaoestadual" placeholder="Inscrição Estadual" value="<?php echo $data_Revendedores[0]['rev_inscricaoestadual']; ?>">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4">
                                                            	<div class="form-group">
                                                                	<label>Inscrição Municipal</label>
                                                                    <div class="input-group"> <span class="input-group-addon"><span <i class="fa fa-pencil-square-o  fa-la"></i></span></span>
                                                                    	<input type="text" class="form-control" name="rev_inscricaomunicipal" id="rev_inscricaomunicipal" placeholder="Inscrição Municipal" value="<?php echo $data_Revendedores[0]['rev_inscricaomunicipal']; ?>">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
														<div class="row">
                                                        	<div class="col-lg-6 col-md-6">
                                                            	<div class="form-group">
                                                                	<label>Grupo Revendedores</label>
                                                                    <div class="input-group">
                                                                    	<span class="input-group-addon"><span class="fa fa-list fa-la"></span></span>
                                                                        <select id="idGrpRevKey" name="idGrpRevKey" class="selectpicker form-control" data-live-search="false" placeholder="Grupo Revendedores" title="Escolha o Grupo Revendedores..." ></select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div> <!-- id="tab1" -->
                                                    <div class="tab-pane" id="tab2">
                                 	                	<div class="row">
                                                        	<div class="col-lg-7 col-md-7">
                                                            	<div class="form-group">
                                                                	<label>Endereço</label>
                                                                    <div class="input-group"> <span class="input-group-addon"><span  <i class="fa fa-text-width fa-la"></i></span></span>
                                                                    	<input type="text" class="form-control" name="ree_endereco" id="ree_endereco" placeholder="Endereço" value="<?php echo utf8_encode($data_RevendedoresEndereco[0]['ree_endereco']); ?>" required>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-2 col-md-2">
                                                            	<div class="form-group">
                                                                	<label>Número</label>
                                                                    <div class="input-group"> <span class="input-group-addon"><span  <i class="fa fa-text-width fa-la"></i></span></span>
                                                                    	<input type="text" class="form-control" name="ree_numero" id="ree_numero" placeholder="Nro" value="<?php echo $data_RevendedoresEndereco[0]['ree_numero']; ?>" required>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-3 col-md-3">
                                                            	<div class="form-group">
                                                                	<label>Complemento</label>
                                                                    <div class="input-group"> <span class="input-group-addon"><span  <i class="fa fa-text-width fa-la"></i></span></span>
                                                                    	<input type="text" class="form-control" name="ree_complemento" id="ree_complemento" placeholder="Complemento" value="<?php echo utf8_encode($data_RevendedoresEndereco[0]['ree_complemento']); ?>">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                            							<div class="row">
                                                            <div class="col-lg-6 col-md-6">
                                                              <div class="form-group">
                                                                    <label>Unidade Federação</label>
                                                                    <div id="formUFE" class="input-group"> 
                                                                    	<span class="input-group-addon"><span class="fa fa-list fa-la"></span></span>
                                                                        <select id="idufecodigo" name="idufecodigo" class="selectpicker form-control" data-live-search="false" placeholder="UFE"  title="Escolha a UFE ..." >
                                                                        </select>
                                                                     </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6">
                                                                <div class="form-group">
                                                                    <label>Municipio</label>
                                                                    <div id="formMunicipio" class="input-group"> 
                                                                    	<span class="input-group-addon"><span class="fa fa-outdent fa-la"></span></span>
                                                                        <select id="idmunibge" name="idmunibge" class="selectpicker form-control" data-live-search="false" placeholder="Municipio"  title="Escolha o Municipio ..." >
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>                                

                                 	                	<div class="row">
                                                        	<div class="col-lg-4 col-md-4">
                                                            	<div class="form-group">
                                                                	<label>Bairro</label>
                                                                    <div class="input-group"> <span class="input-group-addon"><span <i class="fa fa-text-width fa-la"></i></span></span>
                                                                    	<input type="text" class="form-control" name="ree_bairro" id="ree_bairro" placeholder="Bairro" value="<?php echo utf8_encode($data_RevendedoresEndereco[0]['ree_bairro']); ?>" required>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-2 col-md-2">
                                                            	<div class="form-group">
                                                                	<label>CEP</label>
                                                                    <div class="input-group"> <span class="input-group-addon"><span <i class="fa fa-text-width fa-la"></i></span></span>
                                                                    	<input type="text" class="form-control" name="cep_codigo" id="cep_codigo" placeholder="CEP"  onKeyPress="FormataCEP(this,event);" value="<?php echo $data_RevendedoresEndereco[0]['cep_codigo']; ?>" required>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-3 col-md-3">
                                                            	<div class="form-group">
                                                                	<label>Telefone</label>
                                                                    <div class="input-group"> <span class="input-group-addon"><span  <i class="fa fa-phone fa-la"></i></span></span>
                                                                    	<input type="text" class="form-control" name="ree_telefone" size=12 maxlength=14 id="ree_telefone" placeholder="Telefone" onKeyPress="formatTelefone(this,event);" value="<?php echo $data_RevendedoresEndereco[0]['ree_telefone']; ?>">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-3 col-md-3">
                                                            	<div class="form-group">
                                                                	<label>Celular</label>
                                                                    <div class="input-group"> <span class="input-group-addon"><span  <i class="fa fa-mobile  fa-lg"></i></span></span>
                                                                    	<input type="text" class="form-control" name="ree_celular" size=12 maxlength=14 id="ree_celular" placeholder="Celular" onKeyPress="formatCelular(this,event);" value="<?php echo $data_RevendedoresEndereco[0]['ree_celular']; ?>">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                 	                	<div class="row">
                                                        	<div class="col-lg-3 col-md-3">
                                                            	<div class="form-group">
                                                                	<label>Latitude</label>
                                                                    <div class="input-group"> <span class="input-group-addon"><span  <i class="fa fa-ellipsis-v "></i></i></span></span>
                                                                    	<input type="text" class="form-control" name="ree_latitude" id="ree_latitude" placeholder="Latitude" value="<?php echo $data_RevendedoresEndereco[0]['ree_latitude']; ?>" >
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-3 col-md-3">
                                                            	<div class="form-group">
                                                                	<label>Longitude</label>
                                                                    <div class="input-group"> <span class="input-group-addon">  <span  <i class="fa fa-ellipsis-h "></i></span></span>
                                                                    	<input type="text" class="form-control" name="ree_longitude" id="ree_longitude" placeholder="Longitude" value="<?php echo $data_RevendedoresEndereco[0]['ree_longitude']; ?>">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
         	                							<div class="row">
                                                        	<div class="col-lg-8 col-md-8">
                                                            	<div class="form-group">
                                                                	<label>Site</label>
                                                                    <div class="input-group"> <span class="input-group-addon"><span style="font-size: 20px;" <i class="fa fa-sitemap fa-la"></i></span></span>
                                                                    	<input type="text" class="form-control" name="ree_url" id="ree_url" placeholder="Site" value="<?php echo $data_RevendedoresEndereco[0]['ree_url']; ?>">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        	<div class="col-lg-8 col-md-8">
                                                            	<div class="form-group">
                                                                	<label>Email</label>
                                                                    <div class="input-group"> <span class="input-group-addon"><span  <i class="fa fa-envelope-o fa-la"></i></span></span>
                                                                    	<input type="text" class="form-control" name="ree_email" id="ree_email" placeholder="Email" value="<?php echo $data_RevendedoresEndereco[0]['ree_email']; ?>">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <p></p>
                                                    </div> <!-- id="tab2" -->
                                                    <div class="tab-pane" id="tab3">
         	                							<div class="row">
                                                        	<div class="col-lg-6 col-md-6">
                                                            	<div class="form-group">
                                                                	<label>Tipo de Contato</label>
                                                                    <div class="input-group"> <span class="input-group-addon"><span <i class="fa fa-filter "></i></span></span>
	                     	                							<form action="">
	                     	                							<?php 
	                     	                					           							
	                     	                							if($data_RevendedoresContato[0]['rec_tipocontato_c_r_t']=='R')
	                     	                							{
	                     	                							    ?>
	    	                                                                  <input type="radio" name="rec_tipocontato_c_r_t" value="R" checked > Representante Legal ( quem assina pela empresa )<br>
	        	                                                              <input type="radio" name="rec_tipocontato_c_r_t" value="C" > Contato<br> 
	    	                                                                  
	                     	                							    <?php  
	                     	                							}
	                     	                							elseif($data_RevendedoresContato[0]['rec_tipocontato_c_r_t']=='C')
	                     	                							{
	                     	                							    ?>
	    	                                                                  <input type="radio" name="rec_tipocontato_c_r_t" value="R" > Representante Legal ( quem assina pela empresa )<br>
	        	                                                              <input type="radio" name="rec_tipocontato_c_r_t" value="C" checked> Contato<br> 
	                     	                							    <?php 
	                     	                							}
	                     	                							else
	                     	                							{
	                     	                							    ?>
	    	                                                                  <input type="radio" name="rec_tipocontato_c_r_t" value="R" > Representante Legal ( quem assina pela empresa )<br>
	        	                                                              <input type="radio" name="rec_tipocontato_c_r_t" value="C" > Contato<br> 
	                     	                							    <?php 
	                     	                							}
	                     	                							?>
	                     	                							
                	                                                    </form>
                	                                                </div>
         	                									</div>
         	                								</div>	
         	                							</div>
         	                							<div class="row">
                                                        	<div class="col-lg-6 col-md-6">
                                                            	<div class="form-group">
                                                                	<label>Nome</label>
                                                                    <div class="input-group"> <span class="input-group-addon"><span <i class="fa fa-user fa-la"></i></span></span>
                                                                    	<input type="text" class="form-control" name="rec_nome" id="rec_nome" placeholder="Nome" value="<?php echo utf8_encode($data_RevendedoresContato[0]['rec_nome']); ?>" required>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        	<div class="col-lg-5 col-md-5">
                                                            	<div class="form-group">
                                                                	<label>Função</label>
                                                                    <div class="input-group"> <span class="input-group-addon"><span <i class="fa fa-wrench fa-la"></i></span></span>
                                                                    	<input type="text" class="form-control" name="rec_funcao" id="rec_funcao" placeholder="Cargo / Função" value="<?php echo utf8_encode($data_RevendedoresContato[0]['rec_funcao']); ?>" >
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
         	                							<div class="row">
                                                        	<div class="col-lg-3 col-md-3">
                                                            	<div class="form-group">
                                                                	<label>Telefone</label>
                                                                    <div class="input-group"> <span class="input-group-addon"><span  <i class="fa fa-phone fa-la"></i></span></span>
                                                                    	<input type="text" class="form-control" name="rec_telefone" size=12 maxlength=14 id="rec_telefone" placeholder="Telefone" onKeyPress="formatTelefone(this,event);" value="<?php echo $data_RevendedoresContato[0]['rec_telefone']; ?>" >
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        	<div class="col-lg-2 col-md-2">
                                                            	<div class="form-group">
                                                                	<label>Ramal</label>
                                                                    <div class="input-group"> <span class="input-group-addon"><span <i class="fa fa-dot-circle-o fa-la"></i></span></span>
                                                                    	<input type="text" class="form-control" name="rec_ramal" id="rec_ramal" placeholder="Ramal" value="<?php echo $data_RevendedoresContato[0]['rec_ramal']; ?>" >
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                        	<div class="col-lg-3 col-md-3">
                                                            	<div class="form-group">
                                                                	<label>Celular</label>
                                                                    <div class="input-group"> <span class="input-group-addon"><span <i class="fa fa-mobile fa-lg"></i></span></span>
                                                                    	<input type="text" class="form-control" name="rec_celular" size=12 maxlength=14 id="rec_celular" placeholder="Celular" onKeyPress="formatCelular(this,event);" value="<?php echo $data_RevendedoresContato[0]['rec_celular']; ?>" >
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
         	                							<div class="row">
                                                        	<div class="col-lg-8 col-md-8">
                                                            	<div class="form-group">
                                                                	<label>Email</label>
                                                                    <div class="input-group"> <span class="input-group-addon"><span  <i class="fa fa-envelope-o fa-la"></i></span></span>
                                                                    	<input type="text" class="form-control" name="rec_email" id="rec_email" placeholder="Email" value="<?php echo $data_RevendedoresContato[0]['rec_email']; ?>">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <p></p>
                                                        <div class="row">
                                             				<div class="col-lg-12 col-md-12">
                                                				<div class="form-group">
                                                    				<button type="submit" id="btnSalvarDados" class="btn btn-default"><span class="fa fa-save fa-la"></span> Salvar Dados</button>
																	<button type="submit" id="btnNovoDados"   class="btn btn-default"><span class="fa fa-file-o  fa-lg"></span> Novo </button>
                                                    			</div>
                                                			</div>
                                             			</div>
                                                        
                                                    </div> <!-- tab=3 -->
                                                </div> <!-- "tab-content" -->
											 </div> <!-- "tabbable" -->
                                             <br><br><br>
                                             <!--  
                                             <div class="row">
                                             	<div class="col-lg-12 col-md-12">
                                                	<div class="form-group">
                                                    	<button type="submit" id="btnSalvarDados" class="btn btn-default"><span class="fa fa-save fa-la"></span> Salvar Dados</button>
                                                    </div>
                                                </div>
                                             </div>
                                             -->
										</form> <!-- form -->
                					</div> <!-- "x_panel" -->
              					</div> <!-- "col-md-12 col-sm-12 col-xs-12" -->
            				</div>
		                    <!-- Tabela -->	
          					<div class="row">
              					<div class="col-md-12 col-sm-12 col-xs-12">
                					<div class="x_panel">
                  						<div class="x_title">
                    						<h2>Revendedores Cadastrados <small></small></h2>
                    						<div class="clearfix"></div>
                  						</div>
                  						<div class="x_content">
                    						<div class="table-responsive">
                    						
                      							<table class="table table-striped jambo_table bulk_action">
                        							<thead>
                        							   <!--  -->
                          								<tr class="headings">
                          								<!-- 
                            								<th>
                              									<input type="checkbox" id="check-all" class="flat">
                            								</th>
                            								 -->
                            								<th class="column-title">Código</th>
                            								<th class="column-title">CNPJ</th>
                            								<th class="column-title">Razão Social </th>
                            								<th class="column-title">Fantasia</th>
                            								<th class="column-title">UFE</th>
                            								<th class="column-title">Município</th>
                            								<th class="column-title">Telefone</th>
                            								<th class="column-title">Representante</th>
                            								
                            								<th class="column-title no-link last"><span class="nobr"></span></th>
                            								<th class="bulk-actions" colspan="7" >
                              									<a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                            								</th>
                          								</tr>
                        							</thead>
                        						<tbody>
                        		                <?php
                        		                $wctr=0;
                        		                while($row = $lstRevendedores->fetch_assoc()) 
                                                {   
                                                    $paginaEdita = 'cadastroRevendedores.php?idRevKey='.$row['rev_key'];
                                                    
                                                    $rev_key_lst = $row['rev_key'];
                                                    $rev_cnpj_lst = $row['rev_cnpj'];
                                                    $rev_razaosocial_lst = utf8_encode($row['rev_razaosocial']);
                                                    $rev_apelido_lst = utf8_encode($row['rev_apelido']);
                                                    $ufe_codigo_lst = $row['ufe_codigo'];
                                                    $mun_descricao_lst = $row['mun_descricao'];
                                                    $ree_telefone_lst = $row['ree_telefone'];
                                                    $rec_nome_lst = utf8_encode($row['rec_nome']);
                                                    
                                                    if($wctr==0)
                                                    {
                                                        $wctr=1;
                                                        ?>
                                                        <tr class="even pointer">
                                                        <?php 
                                                    }
                                                    elseif($wctr==1)
                                                    {
                                                        $wctr=0;
                                                        ?>
                                                        <tr class="odd pointer">
                                                        <?php
                                                    }
                        							?>
                                                       <!-- 
                                                        	<td class="a-center ">
                                                            	<input type="checkbox" class="flat" name="table_records">
                                                            </td>
                                                         -->    
                                                            <td class="a-right a-right"><?php echo $rev_key_lst; ?></td>
                                                            <td class="a-right a-right"><?php echo $rev_cnpj_lst; ?></td>
                                                            <td class="a-right a-right"><?php echo $rev_razaosocial_lst; ?></td>
                                                            <td class="a-right a-right"><?php echo $rev_apelido_lst; ?></td>
                                                            <td class="a-right a-right"><?php echo $ufe_codigo_lst; ?></td>
                                                            <td class="a-right a-right"><?php echo $mun_descricao_lst; ?></td>
                                                            <td class="a-right a-right"><?php echo $ree_telefone_lst; ?></td>
                                                            <td class="a-right a-right"><?php echo $rec_nome_lst; ?></td>

                                                            <td id="btnDataGrid" name="DataGrid" class="text-center"><a data-placement="top" data-toggle="tooltip" title="Editar Revendedor" class="text-center" href="<?php echo $paginaEdita; ?>" > <i class="fa fa-pencil-square fa-2x"></i></a></td>
                                                        </tr>
                                                        <?php 
                                                    }
                                                    ?>
                        							</tbody>
                      							</table>
                    						</div>
					                  	</div>
                					</div>
          						</div>
        					</div>
        					<!--Tabela-->
                            <!-- /page content -->
                        </div>
                        <div>   
                            <!-- footer content -->
        					<footer>
          						<div class="pull-right">
            						Copyright © 2018 <a href="https://www.tatix.com.br">Tatix Comércio e Participações</a>
          						</div>
          						<div class="clearfix"></div>
        					</footer>
                            <!-- /footer content -->
    					</div>
				</div>
    		</div>
    	</div>
    <!-- MODAL CHAMADO SUCESSO -->
        <div class="modal fade bs-example-modal-sm" id="dadosSuccess" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
          <div class="modal-dialog modal-sm">
            <div class="modal-content">
            	<div class="modal-header">
                	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <p></p>
                	<h4 class="modal-title text-center" id="myModalLabel"><i class="fa fa-user fa-fw"></i>  Dados alterados com Sucesso!</h4>
              	</div>
            </div>
          </div>
        </div>
         <!-- MODAL CHAMADO EROR -->
        <div class="modal fade bs-example-modal-sm" id="dadosError" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
          <div class="modal-dialog modal-sm">
            <div class="modal-content">
            	<div class="modal-header">
                	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <p></p>
                	<h4 class="modal-title text-center" id="myModalLabel"><i class="fa fa-exclamation fa-fw"></i>  Dados Inválido!</h4>
              	</div>
            </div>
          </div>
        </div>
        </div> <!-- "container body" -->
	</div> <!-- "wrapper" -->

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="../vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="../vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="../vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="../vendors/Flot/jquery.flot.js"></script>
    <script src="../vendors/Flot/jquery.flot.pie.js"></script>
    <script src="../vendors/Flot/jquery.flot.time.js"></script>
    <script src="../vendors/Flot/jquery.flot.stack.js"></script>
    <script src="../vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="../vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="../vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="../vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="../vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="../vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="../vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="../vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="../vendors/moment/min/moment.min.js"></script>
    <script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- Custom Theme Scripts 	-->
    <script src="../build/js/custom.min.js"></script>
   
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
       
    <!-- boostrap ABAS -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
	<script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
 
    <!-- scrips do sistema -->
    <script src="js/formata.js"></script> 
    <script src="js/montaComboUFEMunicipio.js"></script>  
    <script src="js/montaComboGrupoRevendedores.js"></script>
          
    <!-- JAVASCRIPT DA PÁGINA -->
    <script>
    
    	$(function(){

  			$('#btnNovoDados').click(function(){
  	  			
				$('#rev_razaosocial').val("")
				$('#rev_apelido').val("")
				$('#rev_cnpj').val("")
				$('#rev_inscricaoestadual').val("")
				$('#rev_inscricaomunicipal').val("")
				$('#idGrpRevKey').val("")
				
				$('#cep_codigo').val("")
				$('#ree_endereco').val("")
				$('#ree_numero').val("")
				$('#ree_complemento').val("")
				$('#ree_bairro').val("")
				$('#idmunibge').val("")
				$('#idufecodigo').val("")
				$('#ree_telefone').val("")
				$('#ree_celular').val("")
				$('#ree_email').val("")
				$('#ree_latitude').val("")
				$('#ree_longitude').val("")
				$('#ree_url').val("")
				
				$('#rec_telefone').val("")
				$('#rec_ramal').val("")
				$('#rec_nome').val("")
				$('#rec_funcao').val("")
				$('#rec_email').val("")
				$('#rec_funcao').val("")
				$('#rec_tipocontato_c_r_t').val("")
	
	    		setTimeout(function() {
					location.href="cadastroRevendedores.php";
				}, 10);

   			});

    			
    		$('#btnSalvarDados').click(function(){
    				
    			var rev_razaosocial =  $('#rev_razaosocial').val()
    			var rev_cnpj =  $('#rev_cnpj').val()
    			var ree_endereco =  $('#ree_endereco').val()
    			var ree_numero =  $('#ree_numero').val()
     			var cep_codigo =  $('#cep_codigo').val()
     			var rec_nome =  $('#rec_nome').val()
	   			var idufecodigo =  $('#idufecodigo').val()
	   			var idmunibge =  $('#idmunibge').val()
				var idGrpRevKey = $('#idGrpRevKey').val()
 			
    			//Valida Campos
    			if (idGrpRevKey == 0 || idGrpRevKey == "")
    			{
    				alert('Selecione o Grupo de Revendedores!');
    				$('#idGrpRevKey').focus();
    				return false;
    			}
    			if (idufecodigo == 0 || idufecodigo == "")
    			{
    				alert('Selecione a Unidade Federativa!');
    				$('#idufecodigo').focus();
    				return false;
    			}
    			if (idmunibge == 0 || idmunibge == "")
    			{
    				alert('Selecione o Municipio!');
    				$('#idmunibge').focus();
    				return false;
    			}

    			if (rev_razaosocial == "")
    			{
    				alert('O Campo Razão SOcial não pode ficar vazio!');
    				$('#rev_razaosocial').focus();
    				return false;
    			}
    			if (rev_cnpj == "")
    			{
    				alert('O Campo CNPJ não pode ficar vazio!');
    				$('#rev_cnpj').focus();
    				return false;
    			}

    			if (ree_endereco == "")
    			{
    				alert('O Campo Endereço não pode ficar vazio!');
    				$('#ree_endereco').focus();
    				return false;
    			}

    			if (ree_numero == "")
    			{
    				alert('O Campo Numero não pode ficar vazio!');
    				$('#ree_numero').focus();
    				return false;
    			}

    			if (cep_codigo == "")
    			{
    				alert('O Campo CEP não pode ficar vazio!');
    				$('#cep_codigo').focus();
    				return false;
    			}
    			
    			if (rec_nome == "")
    			{
    				alert('O Campo Nome Contatdo não pode ficar vazio!');
    				$('#rec_nome').focus();
    				return false;
    			}
    				
    			var dados = $("#dadosForm").serialize();
			
    			$.ajax({                 
    				type: 'POST',                            
    				url: 'db/cadastroRevendedoresDB.php',                                  
    				data:  dados,
    				dataType: 'json',  
    				success: function(response) {
    					  
        				if (response[0].alterou)
        				{
        					$('#dadosSuccess').modal('show')
        					$('#rev_razaosocial').val("")
        					$('#rev_apelido').val("")
        					$('#rev_cnpj').val("")
        					$('#rev_inscricaoestadual').val("")
        					$('#rev_inscricaomunicipal').val("")
        					$('#idGrpRevKey').val("")
        					
        					
        					$('#cep_codigo').val("")
        					$('#ree_endereco').val("")
        					$('#ree_numero').val("")
        					$('#ree_complemento').val("")
        					$('#ree_bairro').val("")
        					$('#idmunibge').val("")
        					$('#idufecodigo').val("")
        					$('#ree_telefone').val("")
        					$('#ree_celular').val("")
        					$('#ree_email').val("")
        					$('#ree_latitude').val("")
        					$('#ree_longitude').val("")
        					$('#ree_url').val("")
        					
        					$('#rec_telefone').val("")
        					$('#rec_ramal').val("")
        					$('#rec_nome').val("")
        					$('#rec_funcao').val("")
        					$('#rec_email').val("")
        					$('#rec_funcao').val("")
        					$('#rec_tipocontato_c_r_t').val("")
        					
        				}
        				else
        				{
        					$('#dadosError').modal('show')
        				}

    	 			  setTimeout(function() {
							location.href="cadastroRevendedores.php";
						}, 130);
	

  					  
    				},
    				error:function(response){
    			  
    					alert('erro');
    				}
    			}); 
    	   		return false;
    		});

/*   
  */   



   		});
	

    	
	</script>
        
	
  </body>
</html>
