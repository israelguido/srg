<!DOCTYPE html>
<html lang="pt">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>SGR | Sistema de Gerenciamento de Revenda</title>
	<link rel="shortcut icon" type="image/x-icon">

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
	
    <!-- bootstrap-progressbar -->
    <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="../vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="nav-md">
	<?php 
        session_start();
	    
        if ($_SESSION["fun_key"] == "" || $_SESSION["usu_key"] == "" )
            header("Location: login.php");
        
        require_once '../connection_bd/mysqli.php'; 
    
		$usu_key = $_SESSION["usu_key"];
	?>
	<style>
		body{
			padding-top:3px;
		}
	</style>
    <div id="wrapper">

        <?php 
         
        include_once '../dataobject/usuario.php';
        
        $data_usuario=clsUsuario_x_Funcionario_x_Empresa::getUsuario_x_Funcionario_x_Empresa($usu_key);
        
        ?>

		<div class="container body">
      		<div class="main_container">
        		<div class="col-md-3 left_col">
          			<div class="left_col scroll-view">
            			<div class="navbar nav_title" style="border: 0;">
              				<a href="index.html" class="site_title">
              					<!--<img alt="" src="../images/sgr_branco.png">-->
							</a>
            			</div>
                         <!-- menu profile quick info -->
            			<div class="profile clearfix">
              				<div class="profile_pic">
                				<!--<img src="../images/img.jpg" alt="..." class="img-circle profile_img">-->
							</div>
							<div class="profile_info">
                				<span>Olá,</span>
                				<h2><?php echo $data_usuario[0]['fun_nome']; ?></h2>
              				</div>
            			</div>
						<!-- Rotina de Montagem do Menu de Usuario -->
            			<?php 
            			
            			include 'menu.php';
            			
            			?>
            			
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="fa fa-sign-out pull-right" data-toggle="dropdown" aria-expanded="false">
                  </a>
                </li>
                <li role="presentation" class="dropdown">
                  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-envelope-o"></i>
                    <span class="badge bg-green">6</span>
                  </a>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">

		<!-- top tiles -->
          <div class="row tile_count"> 
			<div class="row">
				<div class="col-md-4 col-sm-3 col-xs-6 tile_stats_count">
				  <span class="count_top"> Vendas</span>
				  <div class="count"><i class="fa fa-shopping-cart"></i>  0</div>
				</div>
				<div class="col-md-4 col-sm-3 col-xs-6 tile_stats_count">
				  <span class="count_top"> Ticket Médio</span>
				  <div class="count"><i class="fa fa-bar-chart-o"></i>  0,00</div>
				</div>
				<div class="col-md-4 col-sm-3 col-xs-6 tile_stats_count">
				  <span class="count_top">Receita Bruta</span>
				  <div class="count green"><i class="fa fa-money"></i>  0,00</div>
				</div>
			</div>
          </div>
          <!-- /top tiles -->

          <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Meus Pedidos <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-filter"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Data</a></li>
                          <li><a href="#">Cliente</a></li>
						  <li><a href="#">Pedido</a></li>
                        </ul>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content">

                    <!--<p>Add class <code>bulk_action</code> to table for bulk actions options on row select</p>-->

                    <div class="table-responsive">
                      <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
                            <th>
                              <input type="checkbox" id="check-all" class="flat">
                            </th>
                            <th class="column-title">Pedido</th>
                            <th class="column-title">Data</th>
                            <th class="column-title">Cliente</th>
                            <th class="column-title">Produtos</th>
                            <th class="column-title">Valor</th>
                            <th class="column-title">Origem</th>
							<th class="column-title">Pagamento</th>
							<th class="column-title">Status</th>
                            <th class="column-title no-link last"><span class="nobr"></span>
                            </th>
                            <th class="bulk-actions" colspan="7">
                              <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                            </th>
                          </tr>
                        </thead>

                        <tbody>
                          <tr class="even pointer">
                          </tr>
                        </tbody>
                      </table>
                    </div>
							
						
                  </div>
                </div>

          </div>
          <br />
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <!--<footer>
          <div class="pull-right">
            Copyright © 2018 <a href="https://www.tatix.com.br">Tatix Comércio e Participações</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="../vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="../vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="../vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="../vendors/Flot/jquery.flot.js"></script>
    <script src="../vendors/Flot/jquery.flot.pie.js"></script>
    <script src="../vendors/Flot/jquery.flot.time.js"></script>
    <script src="../vendors/Flot/jquery.flot.stack.js"></script>
    <script src="../vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="../vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="../vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="../vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="../vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="../vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="../vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="../vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="../vendors/moment/min/moment.min.js"></script>
    <script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>
	
  </body>
</html>
