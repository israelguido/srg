<?php

class clsGrupoUsuario_x_Formulario
{
    function getGrupoUsuario_x_Formulario($emp_key,$usu_key)
    {
        global $MySql;
        
        $myResult = $MySql->query("
                    SELECT 
                        * 
                    FROM 
                        usuario
                        INNER JOIN grupousuario ON
                            (usuario.grpusu_key = grupousuario.grpusu_key)
                        INNER JOIN grupousuario_x_formulario ON
                            (usuario.grpusu_key = grupousuario.grpusu_key)
                        INNER JOIN formulario ON
                            (grupousuario_x_formulario.form_key = formulario.form_key)
                        INNER JOIN modulo ON
                            (formulario.mdl_key = modulo.mdl_key)

                    WHERE 
                        usu_key = $MySql->quote($usu_key) AND 
                        usu_senhaativa_s_n = 'S'");
       
        if ($myResult->num_rows > 0)
        {
           /* while($row = $myResult->fetch_assoc())
            {
                $data_menu[] = array(
                    'grpusu_descricao'	=> $row['grpusu_descricao'],
                    'form_titulo' => $row['form_titulo'],
                    'form_text' => $row['form_text'],
                    'form_icom' => $row['form_icom'],
                    'form_href' => $row['form_href'],
                    'mdl_descricao' => $row['mdl_descricao']                    
                );
            }
            */
        }
        return $myResult->fetch_assoc();
    }
    
    
}
