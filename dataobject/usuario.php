<?php

class clsUsuario
{
    function getUsuario_email($email)
    {
        global $MySql;
        
        $myResult = $MySql->query("
                    SELECT 
                        * 
                    FROM 
                        usuario 
                    WHERE 
                        usu_email = $MySql->quote('$email') AND 
                        usu_senhaativa_s_n = 'S'");
       
        if ($myResult->num_rows > 0)
        {
            while($row = $myResult->fetch_assoc())
            {
                $usu_logado[] = array(
                    'usu_key'	=> $row['usu_key'],
                    'usu_senha' => $row['usu_senha']
                );
            }
        }
        return $usu_logado;
    }
    
    function getUsuario_usunome($usu_nome)
    {
        global $MySql;
        
        $myResult = $MySql->query("
                    SELECT
                        *
                    FROM
                        usuario
                    WHERE
                        usu_nome = $MySql->quote('$usu_nome') AND
                        usu_senhaativa_s_n = 'S'");
        
        if ($myResult->num_rows > 0)
        {
            while($row = $myResult->fetch_assoc())
            {
                $usu_logado[] = array(
                    'usu_key'	=> $row['usu_key'],
                    'usu_senha' => $row['usu_senha']
                );
            }
        }
        return $usu_logado;
    }

    function getUsuario_usukey($usu_key)
    {
        global $MySql;
        
        $myResult = $MySql->query("
                    SELECT
                        *
                    FROM
                        usuario
                    WHERE
                        usu_key = $MySql->quote('$usu_key') AND
                        usu_senhaativa_s_n = 'S'");
        
        if ($myResult->num_rows > 0)
        {
            while($row = $myResult->fetch_assoc())
            {
                $usu_logado[] = array(
                    'usu_nome'	=> $row['usu_nome'],
                    'usu_senha' => $row['usu_senha']
                );
            }
        }
        return $usu_logado;
    }
    
    
    
    
    function getUsuario_Senha($fun_key)
    {
        global $MySql;
        
        $myResult = $MySql->query("
            SELECT
                CASE
                    WHEN usuario.usu_datasenha is null THEN
                        date_format(str_to_date(usuario.usu_datacadastro, '%Y-%m-%d %H:%i:%s'), '%d/%m/%Y %H:%i:%s')
                    ELSE
                        date_format(str_to_date(usuario.usu_datasenha, '%Y-%m-%d %H:%i:%s'), '%d/%m/%Y %H:%i:%s')
                END data
            FROM
                usuario_x_funcionario_x_empresa
                INNER JOIN usuario ON
                    (usuario_x_funcionario_x_empresa.usu_key = usuario.usu_key) AND 
                    (usuario.usu_senhaativa_s_n = 'S')
            WHERE
                usuario_x_funcionario_x_empresa.fun_key = $MySql->quote('$fun_key')"); 
        if ($myResult->num_rows > 0)
        {
            while($row = $myResult->fetch_assoc())
            {
                $usu_senha[] = array(
                    'data'	=> $row['data']
                );
            }
        }
        return $usu_senha;
    }
    
    
    //Ins
    function insUsuario($fields_values)
    {
        global $MySql;
        
        $sql = "INSERT INTO usuario VALUES (".$fields_values.")";
        if (mysqli_query($MySql, $sql)) {
            
            $myResult = $MySql->query("
                    SELECT LAST_INSERT_ID(usu_key) AS ID FROM usuario ORDER BY usu_key desc limit 1"
                );
            if ($myResult->num_rows > 0)
            {
                while($row = $myResult->fetch_assoc())
                {
                    $data[] = array(
                        'usu_key'	=> $row['ID']
                    );
                }
                return $data;
            }
        }
    }
    
    //Upd
    function updUsuario($fields_values,$key_values)
    {
        global $MySql;
        
        $sql = "UPDATE usuario SET $fields_values WHERE $key_values";
        $myResult = $MySql->query($sql);
    }
}


class clsUsuario_x_Funcionario_x_Empresa
{
    function getUsuario_x_Funcionario_x_Empresa($usu_key)
    {
        global $MySql;
        
        $myResult = $MySql->query("
                    SELECT DISTINCT
                        *
                    FROM
                        usuario_x_funcionario_x_empresa
                        INNER JOIN funcionario ON
                            (usuario_x_funcionario_x_empresa.fun_key  = funcionario.fun_key)
                        INNER JOIN empresa ON
                            (usuario_x_funcionario_x_empresa.emp_key  = empresa.emp_key)
                        INNER JOIN usuario ON
                            (usuario_x_funcionario_x_empresa.usu_key  = usuario.usu_key)
                        INNER JOIN cargo ON
                            (funcionario.crg_key = cargo.crg_key)
                        INNER JOIN grupousuario ON
                            (usuario.grpusu_key = grupousuario.grpusu_key)
                    WHERE
                        usuario_x_funcionario_x_empresa.usu_key = $MySql->quote($usu_key)");
        
        if ($myResult->num_rows > 0)
        {
            while($row = $myResult->fetch_assoc())
            {
                $result[] = array(
                    'fun_key'	=> $row['fun_key'],
                    'fun_nome'	=> $row['fun_nome'],
                    'crg_descricao' => $row['crg_descricao'],
                    'emp_key' => $row['emp_key'],
                    'emp_razaosocial' => $row['emp_razaosocial'],
                    'grpusu_key' => $row['grpusu_key'],
                    'grpusu_descricao' => $row['grpusu_descricao']
                );
            }
        }
       
        return $result;
    }
   
    function getUsuario_x_Funcionario_x_Empresa_funKey($fun_key)
    {
        global $MySql;
        
        $myResult = $MySql->query("
                    SELECT DISTINCT
                        *
                    FROM
                        usuario_x_funcionario_x_empresa
                    WHERE
                        usuario_x_funcionario_x_empresa.fun_key = $MySql->quote($fun_key)");
        
        if ($myResult->num_rows > 0)
        {
            while($row = $myResult->fetch_assoc())
            {
                $result[] = array(
                    'usu_key'	=> $row['usu_key']
                );
            }
        }
        
        return $result;
    }
    
    
    function insUsuario_x_Funcionario_x_Empresa($fields_values)
    {
        global $MySql;
        
        $sql = "INSERT INTO usuario_x_funcionario_x_empresa VALUES (".$fields_values.")";
        mysqli_query($MySql, $sql);
    }
    
    
    
    
    
    function lstUsuario_x_Funcionario_x_Empresa_funKey($emp_key)
    {
        global $MySql;
        
        
        
        
        
        
        
    }
    
}



?>