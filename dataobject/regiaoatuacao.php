<?php

class clsRegiaoAtuacao
{
    function getRegiaoAtuacao($are_key)
    {
        global $MySql;
        
        $myResult = $MySql->query("
            SELECT
                *
            FROM
                regiaoatuacao
            WHERE
                regiaoatuacao.are_key = $MySql->quote($are_key)");
        
        if ($myResult->num_rows > 0)
        {
            while($row = $myResult->fetch_assoc())
            {
                $result[] = array(
                    'are_key'       => $row['are_key'],
                    'are_descricao' => $row['are_descricao']
                );
            }
        }
        return $result;
        
    }
    
    //Ins
    function insRegiaoAtuacao($fields_values)  
    {
        global $MySql;
         
        $sql = "INSERT INTO regiaoatuacao VALUES (".$fields_values.")";
        if (mysqli_query($MySql, $sql)) {
            
            $myResult = $MySql->query("
                    SELECT LAST_INSERT_ID(are_key) AS ID FROM regiaoatuacao ORDER BY are_key desc limit 1"
                );
            if ($myResult->num_rows > 0)
            {
                while($row = $myResult->fetch_assoc())
                {
                    $data[] = array(
                        'are_key'	=> $row['ID']
                    );
                }
                return $data;
            }
        }
    }
    
    
    //Upd
    function updRegiaoAtuacao($fields_values,$key_values)  
    {
        global $MySql;
         
        $sql = "UPDATE regiaoatuacao SET $fields_values WHERE $key_values";
        $myResult = $MySql->query($sql);
    }
}

?>