<?php

class clsFuncionario
{
    function getFuncionario($fun_cpf)
    {
        global $MySql;
        
        $myResult = $MySql->query("
            SELECT
                *
            FROM
                funcionario
            WHERE
                funcionario.fun_cpf = $MySql->quote('$fun_cpf')");
        
        if ($myResult->num_rows > 0)
        {
            while($row = $myResult->fetch_assoc())
            {
                $result[] = array(
                    'fun_key'       => $row['fun_key'],
                    'fun_nome'      => $row['fun_nome'],
                    'fun_cpf'       => $row['fun_cpf'],
                    'fun_email'     => $row['fun_email'],
                    'fun_email_conf'=> $row['fun_email_conf'],
                );
            }
        }
        return $result;
        
    }
    
    
    
    
    function getFuncionario_usuario($fun_key)
    {
        global $MySql;
        
            $myResult = $MySql->query("
            SELECT
                *
            FROM
                funcionario
                LEFT OUTER JOIN cargo ON
                    (funcionario.crg_key=cargo.crg_key)
                LEFT OUTER JOIN usuario_x_funcionario_x_empresa ON
                    (funcionario.fun_key = usuario_x_funcionario_x_empresa.fun_key)
                LEFT OUTER JOIN usuario ON
                    (usuario_x_funcionario_x_empresa.usu_key = usuario.usu_key) AND
                    (usuario.usu_senhaativa_s_n='S')
                LEFT OUTER JOIN grupousuario ON
                    (usuario.grpusu_key = grupousuario.grpusu_key)
            WHERE
                funcionario.fun_key = $MySql->quote($fun_key)");
        
        if ($myResult->num_rows > 0)
        {
            while($row = $myResult->fetch_assoc())
            {
                $result[] = array(
                    'fun_key'       => $row['fun_key'],
                    'fun_nome'      => $row['fun_nome'],
                    'fun_cpf'       => $row['fun_cpf'],
                    'fun_email'     => $row['fun_email'],
                    'fun_email_conf'=> $row['fun_email_conf'],
                    'crg_key'       => $row['crg_key'],
                    'crg_descricao' => $row['crg_descricao'],
                    'grpusu_key'    => $row['grpusu_key'],
                    'usu_nome'      => $row['usu_nome']
                );
            }
        }
        return $result;
        
    }
    
    
   /* 
    function getEmpresa_x_Funcionario_x_Usuario($usu_key)
    {
        global $MySql;
        
        $myResult = $MySql->query("
                    SELECT DISTINCT
                        funcionario.fun_nome,
                        cargo.crg_descricao 
                    FROM 
                        funcionario_x_usuario
                        INNER JOIN funcionario ON
                            (funcionario_x_usuario.fun_key  = funcionario.fun_key)
                        INNER JOIN cargo ON
                            (funcionario.crg_key = cargo.crg_key )
                    WHERE 
                        funcionario_x_usuario.usu_key = $MySql->quote($usu_key)");
    
        if ($myResult->num_rows > 0)
        {
            while($row = $myResult->fetch_assoc())
            {
                $result[] = array(
                    'fun_nome'	=> $row['fun_nome'],
                    'crg_descricao' => $row['crg_descricao']
                );
            }
        }
        return $result;
    }
    */
    
    function lstFuncionario($emp_key)
    {
        global $MySql;
        
        $sql = "
    	SELECT
    	  	funcionario.fun_nome,
    	  	funcionario.fun_cpf,
    	  	funcionario.fun_email,
            cargo.crg_descricao,
            grpusu_descricao,
    	  	funcionario.fun_key,
            usuario.usu_key
    	FROM
    		funcionario
    		INNER JOIN cargo ON
    			(funcionario.crg_key = cargo.crg_key) 
            INNER JOIN usuario_x_funcionario_x_empresa ON
                (funcionario.fun_key = usuario_x_funcionario_x_empresa.fun_key) AND
                (usuario_x_funcionario_x_empresa.emp_key = $emp_key)
            INNER JOIN usuario ON
                (usuario_x_funcionario_x_empresa.usu_key = usuario.usu_key)
            INNER JOIN grupousuario ON
                (usuario.grpusu_key = grupousuario.grpusu_key)
    	WHERE 
             funcionario.fun_excluido_s_n = 'N' 
    	ORDER BY
    		funcionario.fun_nome";
        
        return $sql;
        
        
    }
    
    //Ins
    function insFuncionario($fields_values)  
    {
        global $MySql;
         
        $sql = "INSERT INTO funcionario VALUES (".$fields_values.")";
        if (mysqli_query($MySql, $sql)) {
            
            $myResult = $MySql->query("
                    SELECT LAST_INSERT_ID(fun_key) AS ID FROM funcionario ORDER BY fun_key desc limit 1"
                );
            if ($myResult->num_rows > 0)
            {
                while($row = $myResult->fetch_assoc())
                {
                    $data[] = array(
                        'fun_key'	=> $row['ID']
                    );
                }
                return $data;
            }
        }
    }
    
    
    //Upd
    function updFuncionario($fields_values,$key_values)  
    {
        global $MySql;
         
        $sql = "UPDATE funcionario SET $fields_values WHERE $key_values";
        $myResult = $MySql->query($sql);
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
   
    
    
}

?>