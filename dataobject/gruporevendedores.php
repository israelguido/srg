<?php

class clsGrupoRevendedores
{

    function lstGrupoRevendedores($emp_key)
    {
        global $MySql;
        
        $sql = "
    	SELECT
            gruporevendedores.grprev_key,
            gruporevendedores.grprev_descricao                        	  	
    	FROM
    		gruporevendedores
    	WHERE
            gruporevendedores.emp_key = $emp_key AND
            gruporevendedores.grprev_excluido_s_n = 'N'
    	ORDER BY
    		gruporevendedores.grprev_descricao";
        
        return $sql;
        
        
    }
    
    function getGrupoRevendedores($grprev_key)
    {
        global $MySql;
        
        $myResult = $MySql->query("
            SELECT
                *
            FROM
                gruporevendedores
            WHERE
                gruporevendedores.grprev_key = $MySql->quote('$grprev_key')");
        
        if ($myResult->num_rows > 0)
        {
            while($row = $myResult->fetch_assoc())
            {
                $result[] = array(
                    'grprev_descricao' => $row['grprev_descricao']                );
            }
        }
        return $result;
        
    }
    
    function insGrupoRevendedores($fields_values)
    {
        global $MySql;
        
        $sql = "INSERT INTO gruporevendedores VALUES (".$fields_values.")";
        if (mysqli_query($MySql, $sql)) {
            
            $myResult = $MySql->query("
                    SELECT LAST_INSERT_ID(grprev_key) AS ID FROM gruporevendedores ORDER BY grprev_key desc limit 1"
                );
            if ($myResult->num_rows > 0)
            {
                while($row = $myResult->fetch_assoc())
                {
                    $data[] = array(
                        'grprev_key'	=> $row['ID']
                    );
                }
                return $data;
            }
        }
    }
    
    //Upd
    function updGrupoRevendedores($fields_values,$key_values)
    {
        global $MySql;
        
        $sql = "UPDATE gruporevendedores SET $fields_values WHERE $key_values";
        $myResult = $MySql->query($sql);
    }
    
}



?>