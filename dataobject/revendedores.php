<?php

class clsrevendedores
{
    
    function lstRevendedores($emp_key)
    {
        global $MySql;
        
        $sql = "
    	SELECT DISTINCT
            revendedores.rev_key,
            revendedores.rev_cnpj,
            revendedores.rev_razaosocial,
            revendedores.rev_apelido,
            municipio.ufe_codigo,
            municipio.mun_descricao,
            revendedoresendereco.ree_telefone,
            revendedorescontato.rec_nome

    	FROM
    		revendedores
            LEFT JOIN revendedoresendereco ON
                (revendedores.rev_key = revendedoresendereco.rev_key) AND
                (revendedoresendereco.ree_excluido_s_n = 'N') 
            LEFT JOIN municipio ON
                (revendedoresendereco.mun_ibge = municipio.mun_ibge)
            LEFT JOIN revendedorescontato ON
                (revendedores.rev_key = revendedorescontato.rev_key) AND
                (revendedorescontato.rec_excluido_s_n = 'N') AND
                (revendedorescontato.rec_tipocontato_c_r_t='R')
    	WHERE
            revendedores.emp_key = $emp_key AND
            revendedores.rev_excluido_s_n = 'N'
    	ORDER BY
    		revendedores.rev_razaosocial";
        
        return $sql;
        
    }
    
    
    function getRevendedores($rev_key)
    {
        global $MySql;
         
        $myResult = $MySql->query("
            SELECT
                *
            FROM
                revendedores
            WHERE
                rev_key = $MySql->quote($rev_key)");

        if ($myResult->num_rows > 0)
        {
            while($row = $myResult->fetch_assoc())
            {
                $result[] = array(
                    'rev_key'               => $row['rev_key'],
                    'rev_cnpj'              => $row['rev_cnpj'],
                    'rev_razaosocial'       => $row['rev_razaosocial'],
                    'rev_apelido'           => $row['rev_apelido'],
                    'rev_observacao'        => $row['rev_observacao'],
                    'rev_inscricaoestadual' => $row['rev_inscricaoestadual'],
                    'rev_inscricaomunicipal'=> $row['rev_inscricaomunicipal'],
                    'rev_url'               => $row['rev_url'],
                    'grprev_key'            => $row['grprev_key'],
                    
                );
            }
      }
        return $result;
        
    }
    
    
    //Ins
    function insrevendedores($fields_values)  
    {
        global $MySql;
         
        
        $sql = "INSERT INTO revendedores VALUES (".$fields_values.")";
        
        if (mysqli_query($MySql, $sql)) {
            
            $myResult = $MySql->query("
                    SELECT LAST_INSERT_ID(rev_key) AS ID FROM revendedores ORDER BY rev_key desc limit 1"
                );
            if ($myResult->num_rows > 0)
            {
                while($row = $myResult->fetch_assoc())
                {
                    $data[] = array(
                        'rev_key'	=> $row['ID']
                    );
                }
                return $data;
            }
        }
    }
    
    function insrevendedores_x_Usuario($fields_values)
    {
        global $MySql;
        
        $sql = "INSERT INTO revendedores_x_usuario VALUES (".$fields_values.")";
        mysqli_query($MySql, $sql);
        
    }
        
    
    //Upd
    function updrevendedores($fields_values,$key_values)  
    {
        global $MySql;
         
        $sql = "UPDATE revendedores SET $fields_values WHERE $key_values";
        $myResult = $MySql->query($sql);
          
    }
    
    function delrevendedores($key_values)
    {
        global $MySql;
        
        $sql = "UPDATE revendedores SET $fields_values WHERE $key_values";
        $myResult = $MySql->query($sql);
    }
    
    function getrevendedoresEndereco($rev_key)
    {
        global $MySql;
        
        $myResult = $MySql->query("
            SELECT
                *
            FROM
                revendedoresendereco
            WHERE
                rev_key = $MySql->quote($rev_key)");
         
        if ($myResult->num_rows > 0)
        {
            while($row = $myResult->fetch_assoc())
            {
                $result[] = array(
                    'ree_key'           => $row['ree_key'],
                    'cep_codigo'        => $row['cep_codigo'],
                    'ree_endereco'      => $row['ree_endereco'],
                    'ree_numero'        => $row['ree_numero'],
                    'ree_complemento'   => $row['ree_complemento'],
                    'ree_bairro'        => $row['ree_bairro'],
                    'mun_ibge'          => $row['mun_ibge'],
                    'ufe_codigo'        => $row['ufe_codigo'],
                    'ree_localizacao'   => $row['ree_localizacao'],
                    'ree_telefone'      => $row['ree_telefone'],
                    'ree_celular'       => $row['ree_celular'],
                    'ree_email'         => $row['ree_email'],
                    'ree_0800'          => $row['ree_0800'],
                    'ree_latitude'      => $row['ree_latitude'],
                    'ree_longitude'     => $row['ree_longitude'],
                    'ree_url'           => $row['ree_url'],
                    'ufe_ibge'           => $row['ufe_ibge'],
                    
                );
            }
        }
        return $result;
        
    }
    
    function getrevendedoresEndereco_revkey($rev_key)
    {
        global $MySql;
        
        $myResult = $MySql->query("
            SELECT
                *
            FROM
                revendedoresendereco
            WHERE
                rev_key = $MySql->quote($rev_key)");
        
        if ($myResult->num_rows > 0)
        {
            while($row = $myResult->fetch_assoc())
            {
                $result[] = array(
                    'ree_key'           => $row['ree_key'],
                    'cep_codigo'        => $row['cep_codigo'],
                    'ree_endereco'      => $row['ree_endereco'],
                    'ree_numero'        => $row['ree_numero'],
                    'ree_complemento'   => $row['ree_complemento'],
                    'ree_bairro'        => $row['ree_bairro'],
                    'mun_ibge'          => $row['mun_ibge'],
                    'ufe_codigo'        => $row['ufe_codigo'],
                    'ree_localizacao'   => $row['ree_localizacao'],
                    'ree_telefone'      => $row['ree_telefone'],
                    'ree_celular'       => $row['ree_celular'],
                    'ree_email'         => $row['ree_email'],
                    'ree_0800'          => $row['ree_0800'],
                    'ree_latitude'      => $row['ree_latitude'],
                    'ree_longitude'     => $row['ree_longitude'],
                    'ree_url'           => $row['ree_url'],
                    'ufe_ibge'           => $row['ufe_ibge'],
                    
                );
            }
        }
        return $result;
        
    }
    
    
    
    
    function getrevendedoresEndereco_valido($rev_key)
    {
        global $MySql;
        
        $myResult = $MySql->query("
            SELECT
                ree_key
            FROM
                revendedoresendereco
            WHERE
                ree_excluido_s_n='N' AND
                rev_key = $MySql->quote($rev_key)");
         if ($myResult->num_rows > 0)
        {
            while($row = $myResult->fetch_assoc())
            {
                $result[] = array(
                    'ree_key'           => $row['ree_key']
                );
            }
        }
        return $result;
        
    }
    
    //Ins
    function insrevendedoresEndereco($fields_values)
    {
        global $MySql;
        
        
        $sql = "INSERT INTO revendedoresendereco VALUES (".$fields_values.")";
        
        if (mysqli_query($MySql, $sql)) {
            
            $myResult = $MySql->query("
                    SELECT LAST_INSERT_ID(ree_key) AS ID FROM revendedoresendereco ORDER BY ree_key desc limit 1"
                );
            if ($myResult->num_rows > 0)
            {
                while($row = $myResult->fetch_assoc())
                {
                    $data[] = array(
                        'ree_key'	=> $row['ID']
                    );
                }
                return $data;
            }
        }
    }
    //Upd
    function updrevendedoresEndereco($fields_values,$key_values)
    {
        global $MySql;
        
        $sql = "UPDATE revendedoresendereco SET $fields_values WHERE $key_values";
        $myResult = $MySql->query($sql);
        
        
    }
    
    
    function getrevendedoresContato_revkey($rev_key)
    {
        global $MySql;
        
        $myResult = $MySql->query("
            SELECT
                *
            FROM
                revendedorescontato
            WHERE
                rev_key = $MySql->quote($rev_key)");

        if ($myResult->num_rows > 0)
        {
            while($row = $myResult->fetch_assoc())
            {
                $result[] = array(
                    'rec_key'               => $row['rec_key'],
                    'rec_cpf'               => $row['rec_cpf'],
                    'rec_rg'                => $row['rec_rg'],
                    'rec_telefone'          => $row['rec_telefone'],
                    'rec_ramal'             => $row['rec_ramal'],
                    'rec_nome'              => $row['rec_nome'],
                    'rec_funcao'            => $row['rec_funcao'],
                    'rec_email'             => $row['rec_email'],
                    'rec_celular'           => $row['rec_celular'],
                    'rec_tipocontato_c_r_t' => $row['rec_tipocontato_c_r_t']
                );
            }
        }
        return $result;
    }
    
    function getrevendedoresContato($rec_key)
    {
        global $MySql;
        
        $myResult = $MySql->query("
            SELECT
                *
            FROM
                revendedorescontato
            WHERE
                rec_key = $MySql->quote($rec_key)");
        
        if ($myResult->num_rows > 0)
        {
            while($row = $myResult->fetch_assoc())
            {
                $result[] = array(
                    'rec_key'               => $row['rec_key'],
                    'rec_cpf'               => $row['rec_cpf'],
                    'rec_rg'                => $row['rec_rg'],
                    'rec_telefone'          => $row['rec_telefone'],
                    'rec_ramal'             => $row['rec_ramal'],
                    'rec_nome'              => $row['rec_nome'],
                    'rec_funcao'            => $row['rec_funcao'],
                    'rec_email'             => $row['rec_email'],
                    'rec_celular'           => $row['rec_celular'],
                    'rec_tipocontato_c_r_t' => $row['rec_tipocontato_c_r_t']
                );
            }
        }
        return $result;
    }
    
    
    
    function getrevendedoresContato_cpf($rec_cpf)
    {
        global $MySql;
        
        $myResult = $MySql->query("
            SELECT
                *
            FROM
                revendedorescontato
            WHERE
                rec_cpf = $MySql->quote('$rec_cpf')");
        if ($myResult->num_rows > 0)
        {
            while($row = $myResult->fetch_assoc())
            {
                $result[] = array(
                    'rec_key'               => $row['rec_key'],
                    'rec_cpf'               => $row['rec_cpf'],
                    'rec_rg'                => $row['rec_rg'],
                    'rec_telefone'          => $row['rec_telefone'],
                    'rec_ramal'             => $row['rec_ramal'],
                    'rec_nome'              => $row['rec_nome'],
                    'rec_funcao'            => $row['rec_funcao'],
                    'rec_email'             => $row['rec_email'],
                    'rec_celular'           => $row['rec_celular'],
                    'rec_tipocontato_c_r_t' => $row['rec_tipocontato_c_r_t']
                );
            }
        }
        return $result;
        
    }
 
    
    //Ins
    function insrevendedoresContato($fields_values)
    {
        global $MySql;
        
        $sql = "INSERT INTO revendedorescontato VALUES (".$fields_values.")";

        if (mysqli_query($MySql, $sql)) {
            
            $myResult = $MySql->query("
                    SELECT LAST_INSERT_ID(rec_key) AS ID FROM revendedorescontato ORDER BY rec_key desc limit 1"
                );
            if ($myResult->num_rows > 0)
            {
                while($row = $myResult->fetch_assoc())
                {
                    $data[] = array(
                        'rec_key'	=> $row['ID']
                    );
                }
                return $data;
            }
        }
    }
    //Upd
    function updrevendedoresContato($fields_values,$key_values)
    {
        global $MySql;
        
        $sql = "UPDATE revendedoresCONTATO SET $fields_values WHERE $key_values";
        $myResult = $MySql->query($sql);
        
        
        
        
    }
    
    
    
    
    
}



?>