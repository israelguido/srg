<?php

class clsCargo
{
    function lstCargo()
    {
        global $MySql;
        
        $myResult = $MySql->query("
                    SELECT DISTINCT
                        * 
                    FROM 
                        cargo 
                    ORDER BY 
                        crg_descricao ASC");
    
        if ($myResult->num_rows > 0)
        {
            while($row = $myResult->fetch_assoc())
            {
                
                $result[] = array(
                    'crg_key'	    => $row['crg_key'],
                    'crg_descricao' => $row['crg_descricao']
                );
            }
        }
        return $result;
    }
}

?>