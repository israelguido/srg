<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class Tatix extends MY_Controller {
		public function __construct(){
			parent::__construct();
			$this->load->model('admin/tatix_model', 'tatix');
		}

		public function index()
        {
            $data['order'] = $this->input->get('order');
            $data['title'] = 'Tatix SGR';
            if (!empty($data['order'])) {
                $data['view'] = 'admin/tatix/detail';
                $data['pedidos'] = $this->tatix->getDetail($data['order']);
                $this->load->view('layout', $data);

            } else {
                $data['view'] = 'admin/tatix/index';
                $data['pedidos'] = $this->tatix->getResale();
                $this->load->view('layout', $data);
            }
        }
	}

?>	