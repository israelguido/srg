<?php
$pedido = $pedidos[0];
?>
<!-- Datatable style -->
<link rel="stylesheet" href="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.css">

<div class="title-page">
    <h2>Ordem de Serviço</h2>
</div>

<section class="content">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Detalhe do Pedido</h3>
        </div>
        <div class="order-number"><i class="fas fa-globe-americas"></i><?= $pedido->Order ?></div>
        <div class="personal-info col-lg-4 col-sm-4 col-md-4 col-xs-12">
            <span class="name"><?=$pedido->Client_Name?></span>
            <span class="address"></span>
            <span class="neighborhood"></span>
            <span class="phone"></span>
            <span class="email"></span>
        </div>
        <div class="installation-info col-lg-4 col-sm-4 col-md-4 col-xs-12">
            <span class="name"><?=$pedido->Client_Name?></span>
            <span class="address"></span>
            <span class="neighborhood"></span>
            <span class="phone"></span>
            <span class="email"></span>
        </div>
        <div class="nf-info col-lg-4 col-sm-4 col-md-4 col-xs-12">
            <span class="issue-date"></span>
            <span class="payment-data"></span>
        </div>

        <br>
        <br>
        <br>
        <br>
        <br>
        <br>


        <!-- /.box-header -->
            <table>
                <tr>
                    <td>Origem: </td><td><?=$pedido->Origin?></td>
                </tr>
                <tr>
                    <td>Pedido:</td><td><?=$pedido->Order?></td>
                </tr>
                <tr>
                    <td>Sequencia: </td><td><?=$pedido->Sequence?></td>
                </tr>
                <tr>
                    <td>Data de Criação: </td><td><?=$pedido->Creation_Date?></td>
                </tr>
                <tr>
                    <td>Nome do Cliente: </td><td><?=$pedido->Client_Name?></td>
                </tr>
                <tr>
                    <td>Ultimo Nome: </td><td><?=$pedido->Client_Last_Name?></td>
                </tr>
                <tr>
                    <td>Documento: </td><td><?=$pedido->Client_Document?></td>
                </tr>
                <tr>
                    <td>E-mail: </td><td><?=$pedido->Email?></td>
                </tr>
                <tr>
                    <td>Telefone: </td><td><?=$pedido->Phone?></td>
                </tr>
                <tr>
                    <td>UF: </td><td><?=$pedido->UF?></td>
                </tr>
                <tr>
                    <td>Cidade: </td><td><?=$pedido->City?></td>
                </tr>
                <tr>
                    <td>Address Identification: </td><td><?=$pedido->Address_Identification?></td>
                </tr>
                <tr>
                    <td>Address Type: </td><td><?=$pedido->Address_Type?></td>
                </tr>
                <tr>
                    <td>Recebedor: </td><td><?=$pedido->Receiver_Name?></td>
                </tr>
                <tr>
                    <td>Rua: </td><td><?=$pedido->Street?></td>
                </tr>
                <tr>
                    <td>Rua: </td><td><?=$pedido->Street?></td>
                </tr>
                <tr>
                    <td>Numero: </td><td><?=$pedido->Number?></td>
                </tr>
                <tr>
                    <td>Complemento: </td><td><?=$pedido->Complement?></td>
                </tr>
                <tr>
                    <td>Bairro: </td><td><?=$pedido->Neighborhood?></td>
                </tr>
                <tr>
                    <td>Referencia: </td><td><?=$pedido->Reference?></td>
                </tr>
                <tr>
                    <td>CEP: </td><td><?=$pedido->Postal_Code?></td>
                </tr>
                <tr>
                    <td>Tipo SLA : </td><td><?=$pedido->SLA_Type?></td>
                </tr>
                <tr>
                    <td>Correio : </td><td><?=$pedido->Courrier?></td>
                </tr>
                <tr>
                    <td>Estimativa de Entrega : </td><td><?=$pedido->Estimate_Delivery_Date?></td>
                </tr>
                <tr>
                    <td>Prazo de Entrega : </td><td><?=$pedido->Delivery_Deadline?></td>
                </tr>
                <tr>
                    <td>Status: </td><td><?=$pedido->Status?></td>
                </tr>
                <tr>
                    <td>Ultima Alteração: </td><td><?=$pedido->Last_Change_Date?></td>
                </tr>
                <tr>
                    <td>Cupom: </td><td><?=$pedido->Coupon?></td>
                </tr>
                <tr>
                    <td>Sistema de Pagamento: </td><td><?=$pedido->Payment_System_Name?></td>
                </tr>
                <tr>
                    <td>Parcelas: </td><td><?=$pedido->Installments?></td>
                </tr>
                <tr>
                    <td>Valor de Pagamento: </td><td><?=$pedido->Payment_Value?></td>
                </tr>
                <tr>
                    <td>QTY por SKU: </td><td><?=$pedido->Quantity_SKU?></td>
                </tr>
                <tr>
                    <td>SKU: </td><td><?=$pedido->ID_SKU?></td>
                </tr>
                <tr>
                    <td>Nome do Produto: </td><td><?=$pedido->SKU_Name?></td>
                </tr>
                <tr>
                    <td>Parcelas: </td><td><?=$pedido->Installments?></td>
                </tr>
                <tr>
                    <td>Parcelas: </td><td><?=$pedido->Installments?></td>
                </tr>
                <tr>
                    <td>Parcelas: </td><td><?=$pedido->Installments?></td>
                </tr>


            </table>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</section>

<pre>
    <?=print_r($pedido)?>
</pre>

<!-- DataTables -->
<script src="<?= base_url() ?>public/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
    $(function () {
        $("#example1").DataTable();
    });
</script>
<script>
    $("#view_users").addClass('active');
</script>

<!---->
<!---->
<!--public 'Category_Ids_Sku' => string '60975473' (length=8)-->
<!--public 'Reference_Code' => string '/6/1/' (length=5)-->
<!--public 'SKU_Name' => string '51682' (length=5)-->
<!--public 'SKU_Value' => string 'Purificador Água Soft Plus 220V/60Hz New Black' (length=47)-->
<!--public 'SKU_Selling_Price' => string '1050' (length=4)-->
<!--public 'SKU_Total_Price' => string '1050' (length=4)-->
<!--public 'SKU_Path' => string '1050' (length=4)-->
<!--public 'Item_Attachments' => string '/purificador-agua-soft-plus-n-220v-60hz-new-black/p' (length=51)-->
<!--public 'List_Id' => null-->
<!--public 'List_Type_Name' => null-->
<!--public 'Service_Price_Selling_Price' => null-->
<!--public 'Shipping_List_Price' => null-->
<!--public 'Shipping_Value' => string '10.72' (length=5)-->
<!--public 'Total_Value' => string '0' (length=1)-->
<!--public 'Discounts_Totals' => string '1050' (length=4)-->
<!--public 'Discounts_Names' => string '0' (length=1)-->
<!--public 'Call_Center_Email' => string 'Frete Grátis - Instalação' (length=28)-->
<!--public 'Call_Center_Code' => null-->
<!--public 'Tracking_Number' => null-->
<!--public 'Host' => null-->
<!--public 'GiftRegistry_ID' => string 'everesttatix' (length=12)-->
<!--public 'Seller_Name' => null-->
<!--public 'Status_TimeLine' => string 'Everest' (length=7)-->
<!--public 'Obs' => string 'Obsolete' (length=8)-->
<!--public 'UtmiPart' => null-->
<!--public 'UtmiCampaign' => null-->
<!--public 'UtmiPage' => null-->
<!--public 'Seller_Order_Id' => null-->
<!--public 'Acquirer' => null-->
<!--public 'Authorization_Id' => null-->
<!--public 'TID' => null-->
<!--public 'NSU' => null-->
<!--public 'Card_First_Digits' => null-->
<!--public 'Card_Last_Digits' => null-->
<!--public 'Payment_Approved_By' => null-->
<!--public 'Cancelled_By' => null-->
<!--public 'Cancellation_Reason' => null-->
<!--public 'Gift_Card_Name' => null-->
<!--public 'Gift_Card_Caption' => null-->
<!--public 'Authorized_Date' => null-->
<!--public 'Corporate_Name' => null-->
<!--public 'Corporate_Document' => null-->
<!--public 'TransactionId' => null-->
<!--public 'PaymentId' => string '1C81BD2E37AE4A98B321660270A2AAF4' (length=32)-->
<!--public 'SalesChannel' => string '33A6D3FBEB9547719D1CDFA7DE33AC64' (length=32)-->
<!--public 'marketingTags' => string '1' (length=1)-->
<!--public 'Delivered' => null-->
<!--public 'SKU_RewardValue' => string 'false' (length=5)-->
<!--public 'Is_Marketplace_cetified' => string '0' (length=1)-->
<!--public 'Is_Checked_In' => string 'NA' (length=2)-->
<!--public 'Currency_Code' => string 'NA' (length=2)-->
<!--public 'Column_87' => string 'BRL' (length=3)-->