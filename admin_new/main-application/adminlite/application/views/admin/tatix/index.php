<!-- Datatable style -->
<link rel="stylesheet" href="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.css">

<div class="infos">
    <div class="col-lg-4 col-sm-4 col-md-4 col-xs-4 grey"><span>Vendas</span><i class="fa fa-shopping-cart"></i><?= count($pedidos) ?></div>
    <div class="col-lg-4 col-sm-4 col-md-4 col-xs-4 grey"><span>Ticket Médio</span><i class="far fa-chart-bar"></i><?= '884,33' ?></div>
    <div class="col-lg-4 col-sm-4 col-md-4 col-xs-4 green"><span class="grey">Receita Bruta</span><i class="far fa-money-bill-alt green"></i><?= '7.959,00' ?></div>
</div>

<section class="content">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Meus Pedidos</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive">
            <table id="example1" class="table table-bordered table-striped ">
                <thead>
                    <tr>
                        <!-- <th>Bairro</th> -->
                        <th>Pedido</th>
                        <th>Compra</th>
                        <th>Prazo Instalação</th>
                        <th>Comprador</th>
                        <th>Valor</th>
                        <th>Pagamento</th>
                        <th>Parcelas</th>
                        <th>Origem</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($pedidos as $row): ?>
                        <?php $name = $row->Client_Name . ' ' . $row->Client_Last_Name; ?>
                        <tr>
                            <td><a href="index?order=<?= $row->Order; ?>"><?= $row->Order; ?></a></td>
                            <td><?= date('d/m/Y', strtotime($row->Creation_Date)); ?></td>
                            <td><?= date('d/m/Y', strtotime($row->Delivery_Deadline)); ?></td>
                            <td><?= $name; ?></td>
                            <td><?= $row->Total_Value; ?></td>
                            <td><?= $row->Payment_System_Name; ?></td>
                            <td><?= 'Parcelas'; ?></td>
                            <td><?= $row->Origin; ?></td>
                            <td><?= $row->Status; ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</section>

<!-- DataTables -->
<script src="<?= base_url() ?>public/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
    $(function () {
        $("#example1").DataTable();
    });
</script>
<script>
    $("#view_users").addClass('active');
</script>
