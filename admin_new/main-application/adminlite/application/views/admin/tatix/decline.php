<!-- Datatable style -->
<link rel="stylesheet" href="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.css">

<section class="content">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Lista de Pedidos Rejeitados</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive">
            <table id="example1" class="table table-bordered table-striped ">
                <thead>
                <tr>
                    <th>Order</th>
                    <th>Nome</th>
                    <th>E-mail</th>
                    <th>Documento</th>
                    <th>SKU</th>
                    <th>Nome do Produto</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($rejeitados as $row): ?>
                    <tr>
                        <td><a href="index?order=<?= $row->Order; ?>"><?= $row->Order; ?></a></td>
                        <td><?= $row->Client_Name; ?></td>
                        <td><?= $row->Email; ?></td>
                        <td><?= $row->Client_Document; ?></td>
                        <td><?= $row->SKU_Name?></td>
                        <td><?= $row->SKU_Value; ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>

            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</section>

<!-- DataTables -->
<script src="<?= base_url() ?>public/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
    $(function () {
        $("#example1").DataTable();
    });
</script>
<script>
    $("#view_users").addClass('active');
</script>        
